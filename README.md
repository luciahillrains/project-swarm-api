# Project Swarm

A Java Spring Boot project that will be the back end for some sort of game.

Check out CHANGELOG.md for what has been implemented so far, as well as ROADMAP.md for the roadmap.

**Current Milestone: Pre-Alpha 2 (Pre-Alpha Bravo)**

If this game is released in any capacity, it will be for free.

