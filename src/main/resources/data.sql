INSERT INTO user_account(username, password, email, is_admin) VALUES
('lolo', '$2a$12$Dwg5.giAW9yYUFO1wL2aJ.QDpq/Y0WpQgwkqPqqdEDQCZdg8h8I5S', 'something@gmail.com', 0),
('another', '$2a$12$Dwg5.giAW9yYUFO1wL2aJ.QDpq/Y0WpQgwkqPqqdEDQCZdg8h8I5S', 'andanother@gmail.com', 1);

INSERT INTO world(name, host, port) VALUES ('Kono Ala', 'http://localhost', 8080);

INSERT INTO player_character(account_id, map_id, first_name, last_name, base_class, race, world_id) VALUES
(1, -1, 'Some', 'Adventurer', 0, 0, 1),
(1, -1, 'Another', 'Adventurer', 2,1, 1);

INSERT INTO api_key(api_key) VALUES
('05911ae6-85eb-4f09-ae02-6754400eb8fc'),
('TESTTEST');

INSERT INTO server_switch(switch_name, active) VALUES
('prerelease', true);

--INSERT INTO reprimand(account_id, level ) VALUES
--(1, 1);