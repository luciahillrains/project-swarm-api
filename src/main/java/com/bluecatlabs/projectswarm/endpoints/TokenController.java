package com.bluecatlabs.projectswarm.endpoints;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.dto.response.token.TokenResponse;
import com.bluecatlabs.projectswarm.exceptions.AlreadyOwnsATokenException;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

/***
 * Controller for token-related endpoints.
 *
 */
@RestController
public class TokenController {
    private final Logger log = LoggerFactory.getLogger(TokenController.class);
    UserService userService;
    TokenService tokenService;
    ApiKeyService apiKeyService;
    @Autowired
    public TokenController(UserService userService,
                           TokenService tokenService,
                           ApiKeyService apiKeyService) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.apiKeyService = apiKeyService;
    }

    /***
     * POST /token -> creates a token for authenticated user.  Tokens allow users to make authorized calls to the
     * gameplay endpoints.
     *
     *  If a user has a banned or suspended account, they cannot obtain a token.
     *
     *  They will get an error back if the passed in API Key is unknown (unless there are no current API Keys set up)
     *  or if they already have a token.
     *
     * @param apiKey user's apiKey
     * @param principal Spring Security Response
     * @return general or token response.
     */
    @PostMapping(value = "/token",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("USER")
    @ResponseStatus(HttpStatus.CREATED)
    public TokenResponse createToken(@RequestHeader("api-key") String apiKey, Principal principal) {
        Optional<UserAccount> user = this.userService.findByUsername(principal.getName());
        String token;
        if(user.isEmpty()) {
            log.error("[500] <POST /token> "+ ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
        if(!apiKeyService.isApiKeyHistoric(apiKey)){
            log.error("[400] <POST /token> "+ ErrorStrings.API_KEY_NOT_FOUND);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorStrings.UNAUTHORIZED_REQUEST);
        }
        if(userService.hasReprimandedAccount(user.get())) {
            log.error("[403] <POST /token> "+ErrorStrings.USER_HAS_REPRIMAND);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ErrorStrings.USER_HAS_REPRIMAND);
        }
        try {
            token = tokenService.generateNewToken(user.get().getId());
        }
        catch(AlreadyOwnsATokenException exception) {
            log.error("[400] <POST /token> "+ErrorStrings.USER_ALREADY_HAS_TOKEN);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorStrings.USER_ALREADY_AUTHORIZED);
        }
        log.info("[201] <GET /token> token generated for user {}.  Token: {}", principal.getName(), token);
        return new TokenResponse(token);
    }

}
