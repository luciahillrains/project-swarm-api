package com.bluecatlabs.projectswarm.endpoints;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.request.useraccounts.NewUserAccountRequest;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.security.UserAuthority;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import com.bluecatlabs.projectswarm.services.servers.ServerSwitchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class UserAccountController {
    private final Logger log = LoggerFactory.getLogger(UserAccountController.class);
    UserService userService;
    ServerSwitchService serverSwitchService;
    ApiKeyService apiKeyService;

    @Autowired
    public UserAccountController(UserService userService,
                                 ServerSwitchService serverSwitchService,
                                 ApiKeyService apiKeyService) {
        this.userService = userService;
        this.serverSwitchService = serverSwitchService;
        this.apiKeyService = apiKeyService;
    }

    /***
     * Creates a new account.
     *
     * Returns an error if an account exists with the same username or the same email.
     *
     *
     *
     * @param request parameters for the new account
     * @return a general response or an error'd response.
     */
    @PostMapping(value="/useraccount",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public GeneralResponse createAccount(@RequestHeader("api-key") String apiKey, @RequestBody NewUserAccountRequest request) {
        if(!apiKeyService.getNewestApiKey().equals(apiKey)) {
            log.info("[403] <POST /useraccount> "+ErrorStrings.API_KEY_NOT_NEWEST);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ErrorStrings.UNAUTHORIZED_REQUEST);
        }
        UserAccount userAccount = mapToUserAccount(request);
        boolean uniqueAccount = userService.saveNewUserAccount(userAccount);
        if(!uniqueAccount) {
            log.info("[400] <POST /useraccount> "+ ErrorStrings.NONUNIQUE_USER);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorStrings.NONUNIQUE_USER);
        }
        log.info("[201] <POST /useraccount> Successfully created a user account.");
        return new GeneralResponse();
    }

    public UserAccount mapToUserAccount(NewUserAccountRequest request) {
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(request.getUsername());
        userAccount.setPassword(request.getPassword());
        userAccount.setEmail(request.getEmail());
        UserAuthority userAuthority = new UserAuthority();
        if(serverSwitchService.isSwitchActive(ServerSwitchService.PRERELEASE_SWITCH)) {
            userAuthority.setPrerelease(true);
        }
        userAccount.setAuthority(userAuthority);
        return userAccount;
    }
}
