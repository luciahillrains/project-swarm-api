package com.bluecatlabs.projectswarm.endpoints.authorized.character;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.characters.CharacterDTO;
import com.bluecatlabs.projectswarm.dto.request.characters.CharacterActionRequest;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import com.bluecatlabs.projectswarm.models.characters.PlayerClass;
import com.bluecatlabs.projectswarm.models.characters.PlayerRace;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.server.World;
import com.bluecatlabs.projectswarm.services.characters.CharacterService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import com.bluecatlabs.projectswarm.services.servers.WorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
public class CharacterEndpoint {
    private static final Logger log = LoggerFactory.getLogger(CharacterEndpoint.class);
    CharacterService characterService;
    UserService userService;

    WorldService worldService;

    @Value("${swarm.limits.regular.characters}")
    private int characterLimitRegularAccount;

    @Value("${swarm.limits.premium.characters}")
    private int characterLimitPremiumAccount;
    @Value("${swarm.limits.limited.characters}")
    private int characterLimitLimitedAccount;



    @Autowired
    public CharacterEndpoint(CharacterService characterService,
                             UserService userService,
                             WorldService worldService) {
        this.characterService = characterService;
        this.userService = userService;
        this.worldService = worldService;
    }

    /***
     * POST /auth/character - creates a character based off of the request.
     * @param request - CharacterDTO request featuring information about the character.
     * @param principal - security principal
     * @return general response or error.
     */
    @PostMapping(value="/auth/character",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("USER")
    @ResponseStatus(HttpStatus.CREATED)
    public GeneralResponse createCharacter(@RequestBody CharacterDTO request, Principal principal) {
        PlayerCharacter character;
        try {
            character = mapRequestToPlayerCharacter(request);
        } catch (IllegalArgumentException ex) {
            log.error("[400] <POST /auth/character> "+ ex.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorStrings.BAD_VALUES_IN_CHARACTER_REQUESTS);
        }
        Optional<UserAccount> userOptional = userService.findByUsername(principal.getName());
        if(userOptional.isEmpty()) {
            log.error("[500] <POST /auth/character> "+ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
        if(isPlayerAboveCharacterLimit(userOptional.get())) {
            log.error("[400] <POST /auth/character> "+ErrorStrings.TOO_MANY_CHARACTERS);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorStrings.TOO_MANY_CHARACTERS);
        }

        UserAccount user = userOptional.get();
        character.setUserAccount(user);
        user.getCharacters().add(character);
        userService.save(user);
        return new GeneralResponse();
    }

    /***
     * GET /auth/characters - return a list of the current user's characters.
     * @param principal security principal
     * @return list of Character DTO or exception
     */
    @GetMapping(value = "/auth/characters")
    public List<CharacterDTO> getCharacterList(Principal principal) {
        Optional<UserAccount> userOptional = userService.findByUsername(principal.getName());
        if(userOptional.isEmpty()) {
            log.error("[500] <POST /auth/character> "+ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
        UserAccount user = userOptional.get();
        return user.getCharacters().stream().map((c) -> {
            CharacterDTO dto = new CharacterDTO();
            dto.setCharacterId(c.getId());
            dto.setFirstName(c.getFirstName());
            dto.setLastName(c.getLastName());
            dto.setClassId(c.getBaseClass().ordinal());
            dto.setRaceId(c.getRace().ordinal());
            dto.setWorldId(c.getWorld().getId());
            return dto;
        }).toList();
    }

    /****
     * POST /auth/character/location - moves character to a specific map
     *
     * @param request generic character request, must have character id and a valid map id (higher or equal to 0, upper range to be determined later)
     * @param principal security principal
     * @return generic response
     */
    @PostMapping(value = "/auth/character/location",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("USER")
    public GeneralResponse moveCharacterToMap(@RequestBody CharacterActionRequest request, Principal principal) {
        Optional<UserAccount> account = userService.findByUsername(principal.getName());
        if(account.isEmpty()) {
            log.error("[500] <POST /auth/character/location> "+ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
        Optional<PlayerCharacter> character = account.get().getCharacter(request.getCharacterId());
        if(character.isEmpty()) {
            log.error("[401] <POST /auth/character/location> "+ErrorStrings.CHARACTER_NOT_OWNED_BY_USER);
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ErrorStrings.UNAUTHORIZED_REQUEST);
        }
        if(request.getMapId() < 0) {
            log.error("[400] <POST /auth/character/location> "+ErrorStrings.MAP_ID_INCORRECT);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorStrings.BAD_VALUES_IN_CHARACTER_REQUESTS);
        }
        PlayerCharacter actualCharacter = character.get();
        actualCharacter.setMapId(request.getMapId());
        characterService.save(actualCharacter);
        return new GeneralResponse();
    }


    public PlayerCharacter mapRequestToPlayerCharacter(CharacterDTO request) {
        PlayerCharacter playerCharacter = new PlayerCharacter();
        playerCharacter.setFirstName(request.getFirstName());
        playerCharacter.setLastName(request.getLastName());
        if(PlayerClass.values().length <= request.getClassId()) {
            throw new IllegalArgumentException(ErrorStrings.INVALID_PLAYER_CLASS);
        }
        playerCharacter.setBaseClass(PlayerClass.values()[request.getClassId()]);
        if(PlayerRace.values().length <= request.getRaceId()) {
            throw new IllegalArgumentException(ErrorStrings.INVALID_RACE);
        }
        playerCharacter.setRace(PlayerRace.values()[request.getRaceId()]);
        Optional<World> world = worldService.getWorldById(request.getWorldId());
        if(world.isEmpty()) {
            throw new IllegalArgumentException(ErrorStrings.WORLD_NOT_FOUND);
        }
        playerCharacter.setWorld(world.get());
        return playerCharacter;
    }

    public boolean isPlayerAboveCharacterLimit(UserAccount userAccount) {
        return ((userAccount.getAuthority().isTipper() && userAccount.getCharacters().size() > characterLimitPremiumAccount) ||
                userAccount.getCharacters().size() > characterLimitRegularAccount);
    }

}
