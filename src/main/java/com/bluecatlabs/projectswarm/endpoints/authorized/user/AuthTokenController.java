package com.bluecatlabs.projectswarm.endpoints.authorized.user;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.request.characters.CharacterLoginRequest;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

/**
 * Controller for token-related endpoints that have to be authorized.
 *
 */
@RestController
public class AuthTokenController {
    private final Logger log = LoggerFactory.getLogger(AuthTokenController.class);

    UserService userService;
    TokenService tokenService;

    @Autowired
    public AuthTokenController(UserService userService, TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    /**
     *  DELETE /auth/token -> destroys authorized user's token.  They would have to call GET /token to get a new
     *   one for any of the authorized actions.
     * @param principal Spring Security Principal
     * @return an empty general response.
     */
    @DeleteMapping(value = "/auth/token")
    @Secured("USER")
    public GeneralResponse deleteToken(Principal principal) {
        Optional<UserAccount> user = userService.findByUsername(principal.getName());
        if(user.isPresent()) {
            tokenService.deleteToken(user.get().getId());
            log.info("[200] <DELETE /auth/token> token deleted for user {}", principal.getName());
            return new GeneralResponse();
        } else {
            log.error ("[500] <DELETE /auth/token> "+ ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
    }

    /***
     * POST /auth/token -> adds or changes a character that's associated with a token.  If a gameplay endpoint
     * tries to impact a character that was passed in that's not on the token or if the character associated with the
     * character is null, an error will be returned.
     *
     * @param characterLoginRequest - id of the character passed in to be associated
     * @param principal
     * @return success or error.
     */
    @PostMapping(value = "/auth/token",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("USER")
    public GeneralResponse setCharacterToToken(@RequestBody CharacterLoginRequest characterLoginRequest, Principal principal) {
        Optional<UserAccount> user = userService.findByUsername(principal.getName());
        if(user.isEmpty()) {
            log.error("[500] <POST /auth/token> " + ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
        UserAccount userAccount = user.get();
        Optional<Token> token = tokenService.getTokenByUserId(userAccount.getId());
        if(token.isEmpty()) {
            log.error("[401] <POST /auth/token> "+ErrorStrings.NO_TOKEN);
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ErrorStrings.UNAUTHORIZED_REQUEST);
        }
        Token actualToken = token.get();
        Optional<PlayerCharacter> playerCharacter = userAccount.getCharacter(characterLoginRequest.getCharacterId());
        if(playerCharacter.isEmpty()) {
            log.error("[401] <POST /auth/token> " + ErrorStrings.CHARACTER_NOT_OWNED_BY_USER);
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ErrorStrings.UNAUTHORIZED_REQUEST);
        }
        actualToken.setCharacter(playerCharacter.get());
        tokenService.saveToken(actualToken);
        return new GeneralResponse();
    }
}
