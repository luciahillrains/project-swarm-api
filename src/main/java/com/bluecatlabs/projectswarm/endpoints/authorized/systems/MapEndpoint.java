package com.bluecatlabs.projectswarm.endpoints.authorized.systems;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.map.MapDTO;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@RestController
public class MapEndpoint {
    private final Logger log = LoggerFactory.getLogger(MapEndpoint.class);
    UserService userService;

    TokenService tokenService;

    @Autowired
    public MapEndpoint(UserService userService,
                       TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    /***
     * GET /auth/map returns a poll containing updates the current map or chat.
     * @param characterId - affected character id
     * @param mapId - affected map id
     * @param principal - security princiap
     * @return MapDTO, information related to map and game.
     */
    @GetMapping("/auth/map")
    @Secured("USER")
    public MapDTO getMapInformation(@RequestParam long characterId,
                                    @RequestParam int mapId,
                                    Principal principal) {
        UserAccount loggedInUserAccount = userService.findByUsername(principal.getName()).orElse(new UserAccount());

        if(loggedInUserAccount.getCharacter(characterId).isEmpty()) {
            log.error("[403] <GET /auth/map?> "+ ErrorStrings.CHARACTER_NOT_IN_PLAY);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ErrorStrings.UNAUTHORIZED_REQUEST);
        }

        Token token = tokenService.getTokenByUserId(loggedInUserAccount.getId()).orElse(new Token());
        if(token.getCharacter() == null || token.getCharacter().getId() != characterId) {
            log.error("[403] <GET /auth/map> "+ ErrorStrings.CHARACTER_NOT_OWNED_BY_USER);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ErrorStrings.UNAUTHORIZED_REQUEST);
        }
        return new MapDTO();
    }

}
