package com.bluecatlabs.projectswarm.endpoints.authorized.user;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.useraccounts.UserAccountResponse;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

@RestController
public class AuthUserAccountController {
    private final Logger log = LoggerFactory.getLogger(AuthUserAccountController.class);
    UserService userService;

    @Autowired
    public AuthUserAccountController(UserService userService) {
        this.userService = userService;
    }

    /***
     * Authorized endpoint to get token's user account information.  Returns username and email.
     * @param userPrincipal - user principal supplied by Spring Security.
     * @return a UserAccountResponse of user account information.
     */
    @GetMapping(value = "/auth/account")
    @Secured("USER")
    public UserAccountResponse getAccountDetails(Principal userPrincipal) {
        Optional<UserAccount> userAccountOpt = userService.findByUsername(userPrincipal.getName());
        if(userAccountOpt.isEmpty()) {
            log.error("[500] <GET /auth/account> "+ ErrorStrings.BAD_USER_IN_PRINCIPAL);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorStrings.BAD_USER_IN_PRINCIPAL);
        }
        UserAccount userAccount = userAccountOpt.get();
        UserAccountResponse userAccountResponse = new UserAccountResponse();
        userAccountResponse.setUsername(userAccount.getUsername());
        userAccountResponse.setEmail(userAccount.getEmail());
        log.info("[200] <GET /auth/account> Returning user account " + userAccountResponse);
        return userAccountResponse;
    }
}
