package com.bluecatlabs.projectswarm.endpoints;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.server.BackendInfoResponse;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.servers.WorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ServerController {
    private final Logger log = LoggerFactory.getLogger(ServerController.class);
    ApiKeyService apiKeyService;

    WorldService worldService;

    @Autowired
    public ServerController(ApiKeyService apiKeyService, WorldService worldService) {
        this.apiKeyService = apiKeyService;
        this.worldService = worldService;
    }

    @Value("${swarm.version}")
    private String backendVersion;

    @Value("${swarm.supports}")
    private String uiVersionSupported;

    @Value("${swarm.buildName}")
    private String buildName;
    /***
     * Returns information regarding the current version and state of the
     * Project Swarm overall backend.
     *
     * Such information are current server build, build name, and whether client is supported.
     * @return BackendInfoResponse - server build, whether client updates and world data.
     */
    @GetMapping(value = "/backend")
    public BackendInfoResponse getBackendInfo(@RequestHeader("api-key") String apiKey, @RequestParam String clientVersion) {
        if(!apiKeyService.isApiKeyHistoric(apiKey)) {
            log.error("[403] <GET /backend> "+ErrorStrings.API_KEY_NOT_FOUND);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ErrorStrings.UNAUTHORIZED_REQUEST);
        }

        BackendInfoResponse response = new BackendInfoResponse();
        response.setBackendBuild(backendVersion);
        response.setBuildName(buildName);
        response.setClientNotSupported(!uiVersionSupported.equals(clientVersion));
        response.setWorldList(worldService.getAllWorlds());
        return response;
    }
}
