package com.bluecatlabs.projectswarm.services.login;

import com.bluecatlabs.projectswarm.models.security.Reprimand;
import com.bluecatlabs.projectswarm.models.security.ReprimandLevel;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.repositories.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/***
 * A service that interacts with the user repository.
 */
@Service
public class UserService {
    UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<UserAccount> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Determines whether the UserAccount has a reprimanded account.
     *
     * Reprimands represent bans and suspensions.
     *
     * There is a side effect - we'll toggle reprimand to be inactive if this method is called
     * after the reprimand expires.
     *
     * @param user - authenticated user account.
     * @returns whether they have areprimanded account.
     */
    public boolean hasReprimandedAccount(UserAccount user) {
        Optional<Reprimand> reprimand = user.getActiveReprimandOfHighestLevel();
        if(reprimand.isPresent()) {
            Reprimand activeReprimand = reprimand.get();
            if(activeReprimand.getLevel() == ReprimandLevel.LEVEL_1
                    || activeReprimand.getLevel() == ReprimandLevel.LEVEL_2
                    || activeReprimand.getLevel() == ReprimandLevel.LEVEL_3) {

                if(isCurrentTimeAfterUnbanDate(activeReprimand)) {
                    activeReprimand.setActive(false);
                    userRepository.save(user);
                    return false;
                }
                return true;

            } else {
                return true;
            }
        }
        return false;
    }

    /***
     * Determines whether the time this method is called is after the expiration date of the reprimand.
     * @param reprimand authenticated user's reprimand
     * @return whether the method call date is after the expiration date of the reprimand.
     */
    public boolean isCurrentTimeAfterUnbanDate(Reprimand reprimand) {
        Calendar unbanDate = Calendar.getInstance();
        unbanDate.setTime(reprimand.getCreated());
        unbanDate.add(Calendar.DAY_OF_YEAR, reprimand.getLevel().getDaysBanned());

        return Calendar.getInstance().after(unbanDate);
    }

    public boolean saveNewUserAccount(UserAccount userAccount) {
        List<UserAccount> existingAccounts = userRepository.findByUsernameOrEmail(userAccount.getUsername(),
                userAccount.getEmail());
        if(!existingAccounts.isEmpty()) {
            return false;
        }
        UserAccount savedAccount = userRepository.save(userAccount);
        System.out.println(savedAccount);
        //might change this based on savedAccount output
        return true;
    }

    public UserAccount save(UserAccount userAccount) {
        return userRepository.save(userAccount);
    }
}
