package com.bluecatlabs.projectswarm.services.login;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.exceptions.AlreadyOwnsATokenException;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.repositories.security.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/***
 * A service that interacts with the token repository.
 */
@Service
public class TokenService {
    private final Logger log = LoggerFactory.getLogger(TokenService.class);
    TokenRepository tokenRepository;

    @Autowired
    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    /***
     * Generates a new token for the current user.
     *
     * @param userId id of the authenticated user.
     * @return the token generated.
     * @throws AlreadyOwnsATokenException if the user already has a token, throw exception.
     */
    public String generateNewToken(long userId) throws AlreadyOwnsATokenException {
        UUID uuid = java.util.UUID.randomUUID();
        if(currentTokenExists(userId)) {
            throw new AlreadyOwnsATokenException(ErrorStrings.USER_ALREADY_HAS_TOKEN);
        }
        Token token = new Token(userId, uuid.toString());
        tokenRepository.save(token);
        return uuid.toString();
    }

    /***
     * Called from generateNewToken, determines whether the user has a token.
     * @param userId authenticated user id
     * @return whether or not the user has a token.
     */
    public boolean currentTokenExists(long userId) {
        return tokenRepository.findByUserId(userId).isPresent();
    }

    /***
     * Deletes a token.  Used primarily in DELETE /auth/token
     * @param userId authenticated user id.
     */
    public void deleteToken(long userId) {
        Optional<Token> token = tokenRepository.findByUserId(userId);
        if(token.isPresent()) {
            tokenRepository.delete(token.get());
        } else {
            log.info("A token by user {} was not found for deletion!", userId);
        }
    }

    /***
     * Used in the deletion token task.
     *
     * Gets all tokens that haven't been used in the past 30 minutes and delete them.
     */
    public void deleteStaleTokens() {
        Calendar calendar =Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        List<Token> staleTokens = tokenRepository.findAllByLastActionTakenBefore(calendar.getTime());
        tokenRepository.deleteAll(staleTokens);
    }

    public Optional<Token> getTokenByUserId(long userId) {
        return tokenRepository.findByUserId(userId);
    }

    public Token saveToken(Token token) {
        return tokenRepository.save(token);
    }
}
