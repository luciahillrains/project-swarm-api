package com.bluecatlabs.projectswarm.services.login;

import com.bluecatlabs.projectswarm.models.login.ApiKey;
import com.bluecatlabs.projectswarm.repositories.security.ApiKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/***
 * A service that interacts with the ApiKey repository.
 */
@Service
public class ApiKeyService {
    ApiKeyRepository apiKeyRepository;

    public static final String NEED_API_TOKEN = "NEED_API_KEY_TOKEN";

    @Autowired
    public ApiKeyService(ApiKeyRepository apiKeyRepository) {
        this.apiKeyRepository = apiKeyRepository;
    }

    /***
     * Gets the newest API Key.  If there are no API keys found, returns a warning.
     *
     * Note, as of Amethyst Alpha, this would return a wildcard API key in the event there were no APIs in the table.
     * It does not do this anymore.
     * @return newest api key (or wildcard)
     */
    public String getNewestApiKey() {
       ApiKey newest = apiKeyRepository.findFirstByOrderByCreatedDesc().orElse(new ApiKey());
       if(newest.getApiKey() == null || newest.getApiKey().isEmpty()) {
           return NEED_API_TOKEN;
       }

       return newest.getApiKey();
    }

    /**
     * A historic API Key is one that was used in the past, or even the newest one.
     * @param apiUuid the api key uuid that was passed in.
     * @return whether the uuid was used as a key in the past.
     */
    public boolean isApiKeyHistoric(String apiUuid) {
        return apiKeyRepository.findByApiKey(apiUuid).isPresent();
    }
}
