package com.bluecatlabs.projectswarm.services.servers;

import com.bluecatlabs.projectswarm.models.server.World;
import com.bluecatlabs.projectswarm.repositories.servers.WorldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/***
 * Used in controllers to manipulate world data, such as converting the world data
 * to SimplifiedWorldDTO.
 */
@Service
public class WorldService {
    WorldRepository worldRepository;

    @Autowired
    public WorldService(WorldRepository worldRepository) {
        this.worldRepository = worldRepository;
    }

    public Optional<World> getWorldById(long id) {
        return worldRepository.findById(id);
    }

    public List<World> getAllWorlds() {
        return worldRepository.findAll();
    }
}
