package com.bluecatlabs.projectswarm.services.servers;

import com.bluecatlabs.projectswarm.models.server.ServerSwitch;
import com.bluecatlabs.projectswarm.repositories.servers.ServerSwitchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/****
 * Used to access server switches, which represent certain toggable states for the server itself.
 *
 * Example would be a prerelease switch, or perhaps switches to put features in dark.
 *
 * Switches have to be set via the database.
 */
@Service
public class ServerSwitchService {
    public static final String PRERELEASE_SWITCH = "prerelease";

    ServerSwitchRepository switchRepository;

    @Autowired
    public ServerSwitchService(ServerSwitchRepository serverSwitchRepository) {
        this.switchRepository = serverSwitchRepository;
    }
    public boolean isSwitchActive(String name) {
        Optional<ServerSwitch> switchObject = switchRepository.findBySwitchName(name);
        return switchObject.map(ServerSwitch::isActive).orElse(false);
    }

}
