package com.bluecatlabs.projectswarm.services.characters;


import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import com.bluecatlabs.projectswarm.repositories.characters.PlayerCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {
    PlayerCharacterRepository repository;

    @Autowired
    public CharacterService(PlayerCharacterRepository repository) {
        this.repository = repository;
    }

    public PlayerCharacter save(PlayerCharacter character) {
        return this.repository.save(character);
    }

    public List<PlayerCharacter> findAllOnMap(long mapId) {
        return this.repository.findAllByMapId(mapId);
    }
}
