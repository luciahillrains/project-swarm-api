package com.bluecatlabs.projectswarm.services.security;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.security.UserPrincipal;
import com.bluecatlabs.projectswarm.repositories.security.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/***
 * A service that retrieves user details.  Used primarily with Spring Security.
 */
@Service
public class PSUserDetailsService implements UserDetailsService {
    private final Logger log = LoggerFactory.getLogger(PSUserDetailsService.class);
    private final UserRepository userRepository;

    @Autowired
    public PSUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<UserAccount> userAccount = userRepository.findByUsername(username);

        if(userAccount.isEmpty()) {
            throw new UsernameNotFoundException(ErrorStrings.USERNAME_NOT_FOUND);
        }

        return new UserPrincipal(userAccount.get());
    }
}
