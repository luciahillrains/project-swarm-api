package com.bluecatlabs.projectswarm.filters;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Map;
import java.util.HashMap;


/**
 * Filter to authorize users, for gameplay endpoints marked with /auth/*.
 *
 * Gameplay endpoints need to follow two rules:
 * 1. User must have a valid token.
 * 2. Game client must have the newest API key.
 */
@Component
@Order(2)
public class RequirementFilter implements Filter {
    private final Logger log = LoggerFactory.getLogger(RequirementFilter.class);
    UserService userService;
    TokenService tokenService;
    ApiKeyService apiKeyService;

    Map<String, String> whitelist = new HashMap<>() {{
        put("/token", "POST");
        put("/useraccount", "POST");
        put("/backend", "GET");
    }};

    @Autowired
    public RequirementFilter(UserService userService,
                             TokenService tokenService,
                             ApiKeyService apiKeyService) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.apiKeyService = apiKeyService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
        String path = servletRequest.getServletPath();
        if(whitelist.containsKey(path)
                && whitelist.get(path).equals(servletRequest.getMethod())
                || path.startsWith("/h2-console")) {
            chain.doFilter(request, response);
            return;
        }
        Principal principal = servletRequest.getUserPrincipal();
        if(!isApiKeyNewest(servletRequest)) {
            log.error("Filter Error: API Key is not newest.");
            servletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }
        if(!loggedInUserMatchesToken(servletRequest, principal)) {
            log.error("Filter Error: User's token doesn't match.");
            servletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }
        chain.doFilter(request, response);
    }

    /**
     * Called from doFilter, ensures the principal owns the passed in token.
     * @param servletRequest HttpServletRequest from doFilter
     * @param userDetails Spring Security Principal
     * @return whether or not the principal is the owner of the token.
     */
    public boolean loggedInUserMatchesToken(HttpServletRequest servletRequest, Principal userDetails) {
        String username = userDetails.getName();
        String headerToken = servletRequest.getHeader("token");
        Optional<UserAccount> account = userService.findByUsername(username);
        if(account.isEmpty()) {
            log.error("Filter Error: "+ ErrorStrings.BAD_USER_IN_PRINCIPAL);
            return false;
        }
        UserAccount actualAccount = account.get();
        Optional<Token> token = tokenService.getTokenByUserId(actualAccount.getId());
        if(token.isEmpty()) {
            log.error("Filter Error: Token was not created.");
            return false;
        }
        Token foundToken = token.get();
        boolean match = foundToken.getToken().equals(headerToken);
        if(match) {
            Date currentTime = Calendar.getInstance().getTime();
            foundToken.setLastActionTaken(currentTime);
            tokenService.saveToken(foundToken);
        }
        return match;
    }

    /**
     * Called from doFilter that returns true if the passed in apikey is the newest.
     * @param servletRequest HttpServletRequest from dofilter
     * @return whether the api key that was passed in is the newest.
     */
    public boolean isApiKeyNewest(HttpServletRequest servletRequest) {
        String headerApiKey = servletRequest.getHeader("api-key");
        String newestApiKey = apiKeyService.getNewestApiKey();

        return newestApiKey.equals(ApiKeyService.NEED_API_TOKEN) || headerApiKey.equals(newestApiKey);
    }
}
