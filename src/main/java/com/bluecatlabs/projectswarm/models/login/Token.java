package com.bluecatlabs.projectswarm.models.login;

import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Calendar;
import java.util.Date;

/**
 * A token associates an UUID to a user character.  This acts as a bit of a "session" (in name only), and prevents
 * any out of bound requests.  For instance, you wouldn't be able to MoveCharacter without getting a token first.
 *
 * A token also contains a reference to a character associated with the token, meaning that a token request must be
 * used with that specific character.
 */
@Entity
@Data
public class Token {
    @Id
    long userId;
    String token;

    @Basic(optional = false)
    @Column(name="LastActionTaken", insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    Date lastActionTaken;

    @OneToOne
    private PlayerCharacter character;

    public Token() { }

    public Token(long userId, String token) {
        this.userId = userId;
        this.token = token;
        this.lastActionTaken = Calendar.getInstance().getTime();
    }
}
