package com.bluecatlabs.projectswarm.models.login;

import jakarta.persistence.*;
import lombok.Data;
import java.util.Date;

/**
 * Represents an API Key, which basically helps to authorize a client to interact with this server.
 */
@Entity
@Data
public class ApiKey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String apiKey;

    @Basic(optional = false)
    @Column(name="ApiKeyCreated", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
}
