package com.bluecatlabs.projectswarm.models.server;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
/***
 * Represents a world.
 *
 * Not really used currently, but we have the service.
 *
 * Once this code base is modularized and split up (and possibly get popular)
 * We will have the bones to support multiple worlds.
 */
public class World {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String host;

    private int port;
}
