package com.bluecatlabs.projectswarm.models.server;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * A server switch is a switch that represents some sort of toggable state.
 *
 * Example would be a prerelease state or a state that allows us to release a feature dark.
 *
 * Use ServerSwitchService to interact with switches.
 */
@Entity
@Data
public class ServerSwitch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String switchName;

    private boolean active;
}
