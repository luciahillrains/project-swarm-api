package com.bluecatlabs.projectswarm.models.characters;

/**
 * List of available classes.
 *
 * A character's base class determines their stats, skills and traits.
 *
 *A character can learn cross-skills using a guild system in planning.
 * So a fighter can learn a limited number of skills from other classes,
 * but no traits.
 */
public enum PlayerClass {
    KNIGHT, MEDIC, DEVOUT, ELEMENTALIST, FIGHTER, ARCHER, //PUPPETEER, THIEF
}
