package com.bluecatlabs.projectswarm.models.characters;

/***
 * Represents the player's race.  Basically another set of stat
 * modifiers as well as give them a unique trait.
 */
public enum PlayerRace {
    HUMAN,
    ELF,
    BIRD,
    MINI,
    OGRE
}
