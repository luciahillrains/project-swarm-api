package com.bluecatlabs.projectswarm.models.characters;

import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.server.World;
import jakarta.persistence.*;
import lombok.Data;

/***
 * Represents a playable character.
 *
 * Characters are tied to a user account, and have:
 * - first name
 * - last name
 * - base class (which determines stats & traits)
 * - race (small bonus stat increases/decreases & trait)
 */
@Entity
@Data
public class PlayerCharacter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    UserAccount userAccount;

    private String firstName;
    private String lastName;

    private long mapId = -1;

    @Enumerated(EnumType.ORDINAL)
    private PlayerClass baseClass;

    @Enumerated(EnumType.ORDINAL)
    private PlayerRace race;

    @ManyToOne
    @JoinColumn(name = "world_id")
    World world;
}
