package com.bluecatlabs.projectswarm.models.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * User principal is used for Spring Security reasons.
 *
 * A UserPrincipal has a UserAccount and a list of authorities.
 */
@Data
public class UserPrincipal implements UserDetails {
    UserAccount userAccount;

    public UserPrincipal(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        if(userAccount.getAuthority().isAdmin()) {
            grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
        }
        if(userAccount.getAuthority().isGM()) {
            grantedAuthorities.add(new SimpleGrantedAuthority("GAME_MASTER"));
        }
        if(userAccount.getAuthority().isTipper()) {
            grantedAuthorities.add(new SimpleGrantedAuthority("PREMIUM"));
        }
        if(userAccount.getAuthority().isPrerelease()) {
            grantedAuthorities.add(new SimpleGrantedAuthority("PRERELEASE"));
        }
        grantedAuthorities.add(new SimpleGrantedAuthority("USER"));

        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return userAccount.getPassword();
    }

    @Override
    public String getUsername() {
        return userAccount.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
