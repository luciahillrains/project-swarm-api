package com.bluecatlabs.projectswarm.models.security;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

/**
 * A Reprimand represents an account banning or suspension.
 *
 * There are four levels depending on their severity.
 * LEVEL 1 = 3 day suspension
 * LEVEL 2 = 7 day suspension
 * LEVEL 3 = 30 day suspension
 * LEVEL 4 = infinite banning.
 *
 * Reprimands are cleared if their end date depending on level on a successful GET /token call.
 */
@Entity
@Data
public class Reprimand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private UserAccount userAccount;

    @Enumerated(value = EnumType.ORDINAL)
    private ReprimandLevel level;

    @Basic(optional = false)
    @Column(name="reprimand_created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(columnDefinition = "boolean default true")
    private boolean active;
}
