package com.bluecatlabs.projectswarm.models.security;

import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Represents an account that belongs to a user.
 */
@Entity
@Data
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = Long.MIN_VALUE;

    @Column(nullable=false, unique = true)
    private String username;

    private String password;

    @OneToMany(mappedBy = "userAccount", cascade = CascadeType.ALL)
    private List<Reprimand> reprimands = new ArrayList<>();

    @OneToMany(mappedBy = "userAccount", cascade = CascadeType.ALL)
    private List<PlayerCharacter> characters = new ArrayList<>();

    @Column(unique = true, nullable=false)
    private String email;

    @Embedded
    UserAuthority authority;

    public Optional<Reprimand> getActiveReprimandOfHighestLevel() {
        return reprimands.stream().filter(Reprimand::isActive).max(Comparator.comparingInt(r->r.getLevel().ordinal()));
    }

    public Optional<PlayerCharacter> getCharacter(long characterId) {
        List<PlayerCharacter> matchedCharacters = characters.stream().filter((c) -> c.getId() == characterId).toList();
        if(matchedCharacters.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(matchedCharacters.get(0));
        }
    }

}
