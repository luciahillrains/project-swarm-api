package com.bluecatlabs.projectswarm.models.security;

/**
 * Represents a severity of a reprimand.  Levels 1-3 are suspensions, Level 4 is a ban.
 *
 * Although level 4 is set to 7200 days banned, an account with a Level 4 reprimand is infinitey banned.
 */
public enum ReprimandLevel {
    LEVEL_1(3),
    LEVEL_2(7),
    LEVEL_3(30),
    LEVEL_4(7200);

    private final int daysBanned;

    ReprimandLevel(int daysBanned) {
        this.daysBanned = daysBanned;
    }

    public int getDaysBanned() {
        return daysBanned;
    }
}
