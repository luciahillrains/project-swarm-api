package com.bluecatlabs.projectswarm.models.security;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

/*****
 * Represents the authority of the user.
 */
@Embeddable
@Data
public class UserAuthority {
    /***
     * Represents someone who has tipped in the past.
     * There are no plans to monetize the game, but people who tip
     * Should have something extra - so far it is additional two character slots.
     * Might also involve extra quests.
     */
    @Column(columnDefinition = "boolean default false")
    private boolean isTipper;

    /***
     * Represents someone who signed up in prerelease.
     * Plans are that they will get some gifts later on.
     */
    @Column(columnDefinition = "boolean default false")
    private boolean isPrerelease;

    /***
     * Is a Game Master, i.e. have gameplay and customer service
     * abilities.
     */
    @Column(columnDefinition = "boolean default false")
    private boolean isGM;

    /***
     * Is an admin essentially have all roles as well as server
     * related power.
     */
    @Column(columnDefinition = "boolean default false")
    private boolean isAdmin;
}
