package com.bluecatlabs.projectswarm.tasks;

import com.bluecatlabs.projectswarm.services.login.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Removes stale tokens, that is tokens that haven't been used after a certain point in time.
 */
@Component
public class StaleTokenRemovalTask {
    private final Logger log = LoggerFactory.getLogger(StaleTokenRemovalTask.class);
    TokenService tokenService;

    @Autowired
    public StaleTokenRemovalTask(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    /***
     * Delete stale tokens every 30 minutes.
     */
    @Scheduled(fixedDelay = 1800000)
    public void deleteStaleTokens() {
       log.info("Deleting stale tokens....");
        tokenService.deleteStaleTokens();
    }
}
