package com.bluecatlabs.projectswarm.dto.characters;

import lombok.Data;

@Data
public class CharacterDTO {

    private long characterId;
    private String firstName;
    private String lastName;
    private int classId;
    private int raceId;
    private long worldId;
}
