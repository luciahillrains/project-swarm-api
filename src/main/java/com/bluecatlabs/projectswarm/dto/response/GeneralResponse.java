package com.bluecatlabs.projectswarm.dto.response;

import lombok.Data;

@Data
public class GeneralResponse {
    boolean success = true;
}
