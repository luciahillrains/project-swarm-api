package com.bluecatlabs.projectswarm.dto.response.map;

import lombok.Data;

@Data
public class MapDTO {
    private boolean success = true;
}
