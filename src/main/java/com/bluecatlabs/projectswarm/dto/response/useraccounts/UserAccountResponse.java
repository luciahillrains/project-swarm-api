package com.bluecatlabs.projectswarm.dto.response.useraccounts;

import lombok.Data;

/***
 * An object that gets returned from GET </useraccount> that represents
 * account information.
 */
@Data
public class UserAccountResponse {
    private String username;
    private String email;
}
