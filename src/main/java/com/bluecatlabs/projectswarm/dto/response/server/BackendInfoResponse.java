package com.bluecatlabs.projectswarm.dto.response.server;

import java.util.List;

import com.bluecatlabs.projectswarm.models.server.World;
import lombok.Data;

/***
 * Response object that returns technical information about the backend.
 */
@Data
public class BackendInfoResponse {
    private String backendBuild;
    private String buildName;
    private boolean clientNotSupported;

    List<World> worldList;
}
