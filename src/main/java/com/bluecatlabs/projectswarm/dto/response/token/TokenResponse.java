package com.bluecatlabs.projectswarm.dto.response.token;

import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TokenResponse extends GeneralResponse {

    private String token = "";

    public TokenResponse(String token) {
        this.setSuccess(true);
        this.token = token;
    }

}
