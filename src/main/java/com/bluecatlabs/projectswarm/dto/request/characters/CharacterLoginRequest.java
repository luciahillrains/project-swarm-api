package com.bluecatlabs.projectswarm.dto.request.characters;

import lombok.Data;

@Data
public class CharacterLoginRequest {
    private long characterId;
}
