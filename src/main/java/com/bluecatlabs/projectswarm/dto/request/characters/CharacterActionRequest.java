package com.bluecatlabs.projectswarm.dto.request.characters;

import lombok.Data;

@Data
public class CharacterActionRequest {
    private long characterId;
    private long mapId = -1;
}
