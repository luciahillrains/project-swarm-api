package com.bluecatlabs.projectswarm.exceptions;

/**
 * Thrown if a user tries to create a token they already have.
 */
public class AlreadyOwnsATokenException extends Exception{

    public AlreadyOwnsATokenException(String message) {
        super(message);
    }
}
