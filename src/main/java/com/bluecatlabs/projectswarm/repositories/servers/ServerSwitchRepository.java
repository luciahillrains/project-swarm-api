package com.bluecatlabs.projectswarm.repositories.servers;

import com.bluecatlabs.projectswarm.models.server.ServerSwitch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ServerSwitchRepository extends JpaRepository<ServerSwitch, Long> {

    Optional<ServerSwitch> findBySwitchName(String switchName);
}
