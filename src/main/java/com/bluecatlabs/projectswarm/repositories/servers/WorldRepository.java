package com.bluecatlabs.projectswarm.repositories.servers;


import com.bluecatlabs.projectswarm.models.server.World;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WorldRepository extends JpaRepository<World, Long> {

    Optional<World> findById(long id);
}
