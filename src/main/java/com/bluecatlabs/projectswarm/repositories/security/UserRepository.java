package com.bluecatlabs.projectswarm.repositories.security;

import com.bluecatlabs.projectswarm.models.security.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserAccount, Long> {

    Optional<UserAccount> findByUsername(String username);
    List<UserAccount> findByUsernameOrEmail(String username, String email);
}
