package com.bluecatlabs.projectswarm.repositories.security;

import com.bluecatlabs.projectswarm.models.login.Token;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    Optional<Token> findByUserId(long userId);

    /***
     * Used in the token deletion scheduled task.
     * @param before usually the server date.
     * @return a list of tokens with the last action taken before a certain point in time.
     */
    List<Token> findAllByLastActionTakenBefore(Date before);
}
