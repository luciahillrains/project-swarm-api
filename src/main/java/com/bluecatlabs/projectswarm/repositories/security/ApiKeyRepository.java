package com.bluecatlabs.projectswarm.repositories.security;

import com.bluecatlabs.projectswarm.models.login.ApiKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApiKeyRepository extends JpaRepository<ApiKey, Long> {

    Optional<ApiKey> findByApiKey(String apiKey);

    /**
     * I.E. get newest ApiKey.
     * @return an optional of the newest ApiKey
     */
    Optional<ApiKey> findFirstByOrderByCreatedDesc();
}
