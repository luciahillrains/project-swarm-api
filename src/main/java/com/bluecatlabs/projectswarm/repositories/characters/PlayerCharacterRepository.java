package com.bluecatlabs.projectswarm.repositories.characters;

import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlayerCharacterRepository extends JpaRepository<PlayerCharacter, Long> {
    Optional<PlayerCharacter> findByFirstNameAndLastName(String firstName, String lastName);
    Optional<PlayerCharacter> findById(long id);

    List<PlayerCharacter> findAllByMapId(long mapId);
}
