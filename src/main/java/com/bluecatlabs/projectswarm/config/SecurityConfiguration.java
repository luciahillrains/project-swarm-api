package com.bluecatlabs.projectswarm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

/**
 * Security configuration to use with String Security.
 * We whitelist h2-console, everything else needs to have basic-authentication.
 * CSRF is turned off for /h2-console/ and /auth/* endpoints.
 * /auth/* endpoints are endpoints where one must have a token & an api-key.
 */
@Configuration
@EnableMethodSecurity(securedEnabled = true)
public class SecurityConfiguration  {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((authz) -> authz
                        .requestMatchers(antMatcher("/h2-console/**")).permitAll()
                        .requestMatchers(antMatcher("/useraccount")).permitAll()
                        .requestMatchers(antMatcher("/token")).permitAll()
                        .requestMatchers(antMatcher("/backend")).permitAll()
                        .anyRequest().authenticated()
                )
                .httpBasic(Customizer.withDefaults())
                .headers(headers -> headers.frameOptions(Customizer.withDefaults()).disable())
                .csrf(csrf -> csrf.ignoringRequestMatchers(
                        antMatcher("/h2-console/**"),
                        antMatcher("/useraccount"),
                        antMatcher("/token"),
                        antMatcher("/auth/**")))
                .exceptionHandling((exceptionHandling) -> {
                    exceptionHandling.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
                });
        return http.build();
    }

}
