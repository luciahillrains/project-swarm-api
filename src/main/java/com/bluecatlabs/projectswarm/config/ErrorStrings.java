package com.bluecatlabs.projectswarm.config;

public class ErrorStrings {
    public static final String BAD_VALUES_IN_CHARACTER_REQUESTS = "Bad values passed into character request.";
    public static final String BAD_USER_IN_PRINCIPAL = "User logged in not found.  THIS SHOULDN'T HAPPEN.";
    public static final String CHARACTER_NOT_OWNED_BY_USER = "Character not owned by user.";
    public static final String UNAUTHORIZED_REQUEST = "Unauthorized request.";
    public static final String MAP_ID_INCORRECT = "Map ID is incorrect.";

    public static final String USER_HAS_REPRIMAND = "You have a reprimanded account and cannot log in at this time.";
    public static final String NO_TOKEN = "No token.";

    public static final String API_KEY_NOT_FOUND = "API Key not found.";
    public static final String USER_ALREADY_HAS_TOKEN = "User already has a token.";
    public static final String USER_ALREADY_AUTHORIZED = "User already is authorized.";
    public static final String NONUNIQUE_USER = "User with username or email already exists.";

    public static final String USERNAME_NOT_FOUND = "Username was not found.";
    public static final String INVALID_PLAYER_CLASS = "Base class is invalid.";
    public static final String INVALID_RACE = "Race is invalid.";
    public static final String API_KEY_NOT_NEWEST = "API Key is not newest.";

    public static final String CHARACTER_NOT_IN_PLAY = "Affected character not in play.";

    public static final String TOO_MANY_CHARACTERS = "Player has reached their character creation limit.";
    public static final String WORLD_NOT_FOUND = "The world this character is on is not found.";
}
