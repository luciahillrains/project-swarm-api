package com.bluecatlabs.projectswarm.config.metadata;

import com.bluecatlabs.projectswarm.models.characters.PlayerClass;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/***
 * Service that returns class metadata.
 *
 * getClassName() function is mainly for testing.
 */
@Configuration
@PropertySource(value = "classpath:gameprops/class-metadata.properties")
public class ClassMetadata {
    @Value("${names}")
    List<String> names;

    public String getClassName(PlayerClass clazz) {
        return names.get(clazz.ordinal());
    }
}
