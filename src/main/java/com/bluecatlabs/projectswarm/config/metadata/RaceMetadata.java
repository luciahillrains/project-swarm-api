package com.bluecatlabs.projectswarm.config.metadata;

import com.bluecatlabs.projectswarm.models.characters.PlayerClass;
import com.bluecatlabs.projectswarm.models.characters.PlayerRace;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/***
 * Service that returns race metadata.
 * Personality metadata includes stat changes and unique trait.
 *
 */
@Configuration
@PropertySource(value = "classpath:gameprops/race-metadata.properties")
public class RaceMetadata {
    @Value("${names}")
    List<String> names;

    public String getRaceName(PlayerRace race) {
        return names.get(race.ordinal());
    }
}
