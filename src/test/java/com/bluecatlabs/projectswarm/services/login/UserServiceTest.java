package com.bluecatlabs.projectswarm.services.login;

import com.bluecatlabs.projectswarm.models.security.Reprimand;
import com.bluecatlabs.projectswarm.models.security.ReprimandLevel;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.security.UserAccountTest;
import com.bluecatlabs.projectswarm.repositories.security.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    UserRepository mockRepository;

    UserService userService;

    UserAccount userAccount;
    List<Reprimand> reprimandList = new ArrayList<>();

    private static final String TEST_USERNAME = "TEST_USER";
    private static final String TEST_EMAIL = "TEST@EMAIL.COM";

    @BeforeEach
    public void setup() {
        userService = new UserService(mockRepository);
        userAccount = new UserAccount();
        userAccount.setUsername(TEST_USERNAME);
        userAccount.setEmail(TEST_EMAIL);
    }

    @Test
    public void currentTimeAfterUnban_3DayTest() {
        Calendar banCreated = createDateBeforeNow(3);
        Reprimand reprimand1 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_1, banCreated.getTime(), true);
        Reprimand reprimand2 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_2, banCreated.getTime(), true);
        Reprimand reprimand3 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_3, banCreated.getTime(), true);
        Reprimand reprimand4 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_4, banCreated.getTime(), true);
        boolean actual = userService.isCurrentTimeAfterUnbanDate(reprimand1);
        boolean actual2 = userService.isCurrentTimeAfterUnbanDate(reprimand2);
        boolean actual3 = userService.isCurrentTimeAfterUnbanDate(reprimand3);
        boolean actual4 = userService.isCurrentTimeAfterUnbanDate(reprimand4);
        assertTrue(actual, "Level 1 should true be after 3 days.");
        assertFalse(actual2, "Level 2 should False be after 3 days.");
        assertFalse(actual3, "Level 3 should False be after 3 days.");
        assertFalse(actual4, "Level 4 should False be after 3 days.");
    }

    @Test
    public void currentTimeAfterUnban_Level2ShouldBe7DaysAfter() {
        Calendar banCreated = createDateBeforeNow(7);
        Reprimand reprimand1 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_1, banCreated.getTime(), true);
        Reprimand reprimand2 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_2, banCreated.getTime(), true);
        Reprimand reprimand3 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_3, banCreated.getTime(), true);
        Reprimand reprimand4 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_4, banCreated.getTime(), true);
        boolean actual = userService.isCurrentTimeAfterUnbanDate(reprimand1);
        boolean actual2 = userService.isCurrentTimeAfterUnbanDate(reprimand2);
        boolean actual3 = userService.isCurrentTimeAfterUnbanDate(reprimand3);
        boolean actual4 = userService.isCurrentTimeAfterUnbanDate(reprimand4);
        assertTrue(actual, "Level 1 should True be after 7 days.");
        assertTrue(actual2, "Level 2 should True be after 7 days.");
        assertFalse(actual3, "Level 3 should False be after 7 days.");
        assertFalse(actual4, "Level 4 should False be after 7 days.");
    }

    @Test
    public void currentTimeAfterUnban_Level3ShouldBe30DaysAfter() {
        Calendar banCreated = createDateBeforeNow(30);
        Reprimand reprimand1 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_1, banCreated.getTime(), true);
        Reprimand reprimand2 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_2, banCreated.getTime(), true);
        Reprimand reprimand3 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_3, banCreated.getTime(), true);
        Reprimand reprimand4 = UserAccountTest.createReprimand(ReprimandLevel.LEVEL_4, banCreated.getTime(), true);
        boolean actual = userService.isCurrentTimeAfterUnbanDate(reprimand1);
        boolean actual2 = userService.isCurrentTimeAfterUnbanDate(reprimand2);
        boolean actual3 = userService.isCurrentTimeAfterUnbanDate(reprimand3);
        boolean actual4 = userService.isCurrentTimeAfterUnbanDate(reprimand4);
        assertTrue(actual, "Level 1 should True be after 30 days.");
        assertTrue(actual2, "Level 2 should True be after 30 days.");
        assertTrue(actual3, "Level 3 should True be after 30 days.");
        assertFalse(actual4, "Level 4 should false be after 30 days.");
    }

    @Test
    public void hasNonReprimandedAccount_ShouldReturnFalseWithNoReprimands() {
        userAccount.setReprimands(reprimandList);
        boolean actual = userService.hasReprimandedAccount(userAccount);
        assertFalse(actual, "Should Return false.");
    }

    @Test
    public void hasNonReprimandedAccount_ShouldReturnTrueWithLevel4Reprimand() {
        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_4, Calendar.getInstance().getTime(), true));
        userAccount.setReprimands(reprimandList);
        boolean actual = userService.hasReprimandedAccount(userAccount);
        assertTrue(actual, "Should return true.");
    }

    @Test
    public void hasNonReprimandedAccount_ShouldReturnTrueWithLevel4ReprimandEvenMadeYearsAgo() {
        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_4, createDateBeforeNow(9999).getTime(), true));
        userAccount.setReprimands(reprimandList);
        boolean actual = userService.hasReprimandedAccount(userAccount);
        assertTrue(actual, "level 4 reprimands always are true.");
    }

    @Test
    public void hasNonReprimandedAccount_ShouldReturnTrueIfLevel1Level2OrLevel3DatesAreRecent() {
        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_1, Calendar.getInstance().getTime(), true));
        userAccount.setReprimands(reprimandList);
        boolean actualLevel1 = userService.hasReprimandedAccount(userAccount);
        assertTrue(actualLevel1, "Level 1 with date now should be true.");

        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_2, Calendar.getInstance().getTime(), true));
        userAccount.setReprimands(reprimandList);
        boolean actualLevel2 = userService.hasReprimandedAccount(userAccount);
        assertTrue(actualLevel2, "Level 2 with date now should be true.");

        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_3, Calendar.getInstance().getTime(), true));
        userAccount.setReprimands(reprimandList);
        boolean actualLevel3 = userService.hasReprimandedAccount(userAccount);
        assertTrue(actualLevel3, "Level 3 with date now should be true.");

    }

    @Test
    public void hasNonReprimandedAccount_ShouldReturnFalseIfLevel1Level2OrLevel3DatesHaveElapsed() {
        when(mockRepository.save(userAccount)).thenReturn(userAccount);
        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_1, createDateBeforeNow(4).getTime(), true));
        userAccount.setReprimands(reprimandList);
        boolean actualLevel1 = userService.hasReprimandedAccount(userAccount);
        assertFalse(actualLevel1, "Level 1 with 3 days elapsed should be false.");

        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_2, createDateBeforeNow(8).getTime(), true));
        boolean actualLevel2 = userService.hasReprimandedAccount(userAccount);
        assertFalse(actualLevel2, "Level 2 with 7 days elapsed now should be false.");

        reprimandList.add(UserAccountTest.createReprimand(ReprimandLevel.LEVEL_3, createDateBeforeNow(31).getTime(), true));
        boolean actualLevel3 = userService.hasReprimandedAccount(userAccount);
        assertFalse(actualLevel3, "Level 3 with 30 days elapsed now should be false.");

        assertFalse(userAccount.getReprimands().get(0).isActive(), "Level 1 Reprimand active should be false.");
        assertFalse(userAccount.getReprimands().get(1).isActive(), "Level 2 Reprimand active should be false.");
        assertFalse(userAccount.getReprimands().get(2).isActive(), "Level 3 Reprimand active should be false.");
    }

    @Test
    public void saveUserAccount_returnsFalseIfExistingAccountExists() {
        List<UserAccount> userList = new ArrayList<>();
        userList.add(userAccount);
        when(mockRepository.findByUsernameOrEmail(TEST_USERNAME, TEST_EMAIL)).thenReturn(userList);
        boolean actual = userService.saveNewUserAccount(userAccount);
        assertFalse(actual, "Shouldn't be successful due to finding previous account with same username or email");
    }

    @Test
    public void saveUserAccount_returnsTrueIfExistingAccountIsSaved() {
        userAccount.setUsername(TEST_USERNAME);
        userAccount.setEmail(TEST_EMAIL);
        when(mockRepository.findByUsernameOrEmail(TEST_USERNAME, TEST_EMAIL)).thenReturn(new ArrayList<>());
        when(mockRepository.save(userAccount)).thenReturn(userAccount);
        boolean actual = userService.saveNewUserAccount(userAccount);
        assertTrue(actual, "Should be successful it's a unique account!");
    }


    public Calendar createDateBeforeNow(int numberOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, numberOfDays * -1);
        calendar.add(Calendar.HOUR, -6);
        return calendar;
    }
}