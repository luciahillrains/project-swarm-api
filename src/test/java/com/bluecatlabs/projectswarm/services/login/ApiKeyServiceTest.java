package com.bluecatlabs.projectswarm.services.login;

import com.bluecatlabs.projectswarm.models.login.ApiKey;
import com.bluecatlabs.projectswarm.repositories.security.ApiKeyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApiKeyServiceTest {

    @Mock
    ApiKeyRepository mockApiKeyRepository;

    ApiKeyService service;

    ApiKey testApiKey = new ApiKey();

    private static final String TEST_KEY = "TEST_KEY";
    @BeforeEach
    void setup() {
        service = new ApiKeyService(mockApiKeyRepository);
    }

    @Test
    public void getNewestApiKeyReturnsApiKey() {
        testApiKey.setApiKey(TEST_KEY);
        when(mockApiKeyRepository.findFirstByOrderByCreatedDesc()).thenReturn(Optional.of(testApiKey));
        String newestKey = service.getNewestApiKey();
        assertEquals(TEST_KEY, newestKey, "Should return an api key.");
    }

    @Test
    public void getNewestApiKeyReturnsStarIfApiKeyListIsEmpty() {
        when(mockApiKeyRepository.findFirstByOrderByCreatedDesc()).thenReturn(Optional.empty());
        String newestKey = service.getNewestApiKey();
        assertEquals(ApiKeyService.NEED_API_TOKEN, newestKey, "Should return need api string.");
    }

    @Test
    public void getApiKeyHistoricShouldReturnTrueIfFound() {
        when(mockApiKeyRepository.findByApiKey(TEST_KEY)).thenReturn(Optional.of(testApiKey));
        boolean historicKey = service.isApiKeyHistoric(TEST_KEY);
        assertTrue(historicKey, "Historic Key should be true.");
    }

    @Test
    public void getApiKeyHistoricShouldReturnFalseIfNotFOund() {
        when(mockApiKeyRepository.findByApiKey(TEST_KEY)).thenReturn(Optional.empty());
        boolean historicKey = service.isApiKeyHistoric(TEST_KEY);
        assertFalse(historicKey, "Historic key not found should be false.");
    }
}