package com.bluecatlabs.projectswarm.services.login;

import com.bluecatlabs.projectswarm.exceptions.AlreadyOwnsATokenException;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.repositories.security.TokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TokenServiceTest {
    @Mock
    TokenRepository mockRepository;

    TokenService service;
    Token testToken = new Token(1L, "xxx");

    @BeforeEach
    public void setup() {
        service = new TokenService(mockRepository);
    }

    @Test
    public void generateNewToken_ShouldGenerateNewToken() throws AlreadyOwnsATokenException {
        when(mockRepository.save(Mockito.any())).thenReturn(null);
        String token = service.generateNewToken(1L);
        verify(mockRepository, times(1)).save(Mockito.any());
        assertNotNull(token, "Token should not be null");
        assertFalse(token.isEmpty(), "Token length bigger than 0.");
    }

    @Test
    public void generateNewToken_ShouldThrowException() throws AlreadyOwnsATokenException {
        when(mockRepository.findByUserId(1L)).thenReturn(Optional.of(testToken));
        Exception exception = assertThrows(AlreadyOwnsATokenException.class, () -> {
            service.generateNewToken(1L);
        });
    }

    @Test
    public void currentTokenExists_ReturnsTrueIfTokenExists() {
        when(mockRepository.findByUserId(1L)).thenReturn(Optional.of(new Token(1L, "xxx")));
        boolean exists = service.currentTokenExists(1L);
        assertTrue(exists, "Token should exist");
    }

    @Test
    public void currentTokenExists_ReturnsFalseIfTokenDoesNotExist() {
        when(mockRepository.findByUserId(1L)).thenReturn(Optional.empty());
        boolean exists = service.currentTokenExists(1L);
        assertFalse(exists, "Token should not exist");
    }

    @Test
    public void deleteToken_shouldCallDeleteMethod() {
        when(mockRepository.findByUserId(1L)).thenReturn(Optional.of(testToken));
        doNothing().when(mockRepository).delete(testToken);
        service.deleteToken(1L);
        verify(mockRepository, times(1)).delete(testToken);
    }


}