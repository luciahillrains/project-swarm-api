package com.bluecatlabs.projectswarm.services.security;

import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.security.UserAuthority;
import com.bluecatlabs.projectswarm.repositories.security.UserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PSUserDetailsServiceTest {

    @Mock
    UserRepository userRepository;
    UserAccount realUser;

    PSUserDetailsService psUserDetailsService;

    @BeforeEach
    void setup() {
        realUser = new UserAccount();
        realUser.setId(100);
        realUser.setUsername("test");
        realUser.setPassword("password");
        realUser.setAuthority(new UserAuthority());
        psUserDetailsService = new PSUserDetailsService(userRepository);
    }

    @Test
    void testLoadByUsername_ShouldThrowExceptionIfNoUser() {
        when(userRepository.findByUsername("test")).thenReturn(Optional.empty());
        Exception exception = assertThrows(UsernameNotFoundException.class, () -> {
            psUserDetailsService.loadUserByUsername("test");
        });
    }

    @Test
    void testLoadByUsername_ShouldReturnPrincipalIfUser() {
        when(userRepository.findByUsername("test")).thenReturn(Optional.of(realUser));
        UserDetails principal = psUserDetailsService.loadUserByUsername("test");
        assertEquals("test", principal.getUsername(), "Expect UserDetails username to be test.");
        assertEquals("password", principal.getPassword(), "Expect UserDetails password to be password.");
    }


}