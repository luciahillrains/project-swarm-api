package com.bluecatlabs.projectswarm.services.servers;

import com.bluecatlabs.projectswarm.models.server.ServerSwitch;
import com.bluecatlabs.projectswarm.repositories.servers.ServerSwitchRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ServerSwitchServiceTest {
    @Mock
    ServerSwitchRepository mockSwitchRepository;

    ServerSwitchService serverSwitchService;

    private static final String TEST_SWITCH_NAME = "TEST";
    @BeforeEach
    public void setup() {
        this.serverSwitchService = new ServerSwitchService(mockSwitchRepository);
    }

    @Test
    public void isActive_returnsFalseIfNotFound() {
        when(mockSwitchRepository.findBySwitchName(TEST_SWITCH_NAME)).thenReturn(Optional.empty());
        boolean actually = serverSwitchService.isSwitchActive(TEST_SWITCH_NAME);
        assertFalse(actually, "Empty switches should be false.");
    }

    @Test
    public void isActive_returnsTrueIfActive() {
        ServerSwitch serverSwitch = new ServerSwitch();
        serverSwitch.setActive(true);
        when(mockSwitchRepository.findBySwitchName(TEST_SWITCH_NAME)).thenReturn(Optional.of(serverSwitch));
        boolean actually = serverSwitchService.isSwitchActive(TEST_SWITCH_NAME);
        assertTrue(actually, "Active switches should be true.");
    }
}