package com.bluecatlabs.projectswarm.filters;

import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.security.Principal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RequirementFilterTest {
    @Mock
    UserService mockUserService;

    @Mock
    TokenService mockTokenService;

    @Mock
    ApiKeyService mockApiKeyService;

    @Mock
    HttpServletRequest mockHttpServletRequest;

    @Mock
    HttpServletResponse mockHttpServletResponse;

    @Mock
    FilterChain mockFilterChain;

    @Mock
    Principal mockPrincipal;

    RequirementFilter requirementFilter;

    UserAccount userAccount = new UserAccount();
    Token token = new Token();

    private static final String USERNAME = "test";
    private static final String USER_TOKEN ="TESTESTEST1";
    private static final String OTHER_USER_TOKEN2="TESTTESTTEST2";
    private static final String NEWEST_API_KEY="NEW_API_KEY";

    private static final String AUTHORIZED_URL="/auth/x";
    private static final String UNAUTHORIZED_URL="/token";


    @BeforeEach
    public void setup() {
        requirementFilter = new RequirementFilter(mockUserService, mockTokenService, mockApiKeyService);
        userAccount.setUsername("test");
        userAccount.setId(1L);
        token.setUserId(1L);
        token.setToken(USER_TOKEN);
    }


    @Test
    public void loggedInUserMatchesToken_ReturnsFalseIfTokenNotCreated() {
        setUpTokenMocks(USER_TOKEN, null);

        boolean result = requirementFilter.loggedInUserMatchesToken(mockHttpServletRequest, mockPrincipal);
        assertFalse(result, "If user doesn't have a token, this should be false.");
    }

    @Test
    public void loggedInUserMatchesToken_ReturnsFalseIfTokenDoesNotMatch() {
        token.setToken(OTHER_USER_TOKEN2);
        setUpTokenMocks(USER_TOKEN, token);

        boolean result = requirementFilter.loggedInUserMatchesToken(mockHttpServletRequest, mockPrincipal);
        assertFalse(result, "If token doesn't match, return false.");
    }

    @Test
    public void loggedInUserMatchesToken_ReturnsTrueIfTokenDoesMatch() {
        setUpTokenMocks(USER_TOKEN, token);
        when(mockTokenService.saveToken(isA(Token.class))).thenReturn(null);
        boolean result = requirementFilter.loggedInUserMatchesToken(mockHttpServletRequest, mockPrincipal);
        verify(mockTokenService, times(1)).saveToken(isA(Token.class));
        assertTrue(result, "If token does match, return true.");
    }

    @Test
    public void isApiKeyNewest_returnsFalseIfSuppliedApiKeyIsntNewest() {
        setUpApiKeyMocks("NOT_NEW_API_KEY", NEWEST_API_KEY);

        boolean result = requirementFilter.isApiKeyNewest(mockHttpServletRequest);
        assertFalse(result, "If api key isn't newest, return false.");
    }

    @Test
    public void isApiKeyNewest_returnsTrueIfSuppliedApiKey() {
        setUpApiKeyMocks(NEWEST_API_KEY, NEWEST_API_KEY);

        boolean result = requirementFilter.isApiKeyNewest(mockHttpServletRequest);
        assertTrue(result, "If api key is newest, return true.");
    }

    @Test
    public void isApiKeyNewest_ifNoNewestLetUserInDueForApiKey() {
        setUpApiKeyMocks(NEWEST_API_KEY, ApiKeyService.NEED_API_TOKEN);
        boolean result = requirementFilter.isApiKeyNewest(mockHttpServletRequest);
        assertTrue(result, "If newest apikey is star, return true.");
    }
    @Test
    public void filter_ifTokenDoesntMatchSendError() throws ServletException, IOException {
        setUpFilterMocks(AUTHORIZED_URL);
        setUpApiKeyMocks(NEWEST_API_KEY, NEWEST_API_KEY);
        setUpTokenMocks(OTHER_USER_TOKEN2, token);
        requirementFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);
        verify(mockHttpServletResponse, times(1)).sendError(HttpServletResponse.SC_FORBIDDEN);
        verify(mockFilterChain, times(0)).doFilter(mockHttpServletRequest, mockHttpServletResponse);
    }

    @Test
    public void filter_ifApiKeyIsNotNewestSendError() throws ServletException, IOException {
        setUpFilterMocks(AUTHORIZED_URL);
        setUpApiKeyMocks("NOT_NEW_API_KEY", NEWEST_API_KEY);
        requirementFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);
        verify(mockHttpServletResponse, times(1)).sendError(HttpServletResponse.SC_FORBIDDEN);
        verify(mockFilterChain, times(0)).doFilter(mockHttpServletRequest, mockHttpServletResponse);
    }

    @Test
    public void filter_ShouldCallDoFilterAndNotSendErrorIfEverythingOk() throws ServletException, IOException {
        setUpFilterMocks(AUTHORIZED_URL);
        setUpTokenMocks(USER_TOKEN, token);
        setUpApiKeyMocks(NEWEST_API_KEY, NEWEST_API_KEY);
        when(mockTokenService.saveToken(isA(Token.class))).thenReturn(null);
        requirementFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);
        verify(mockTokenService, times(1)).saveToken(isA(Token.class));
        verify(mockHttpServletResponse, times(0)).sendError(HttpServletResponse.SC_FORBIDDEN);
        verify(mockFilterChain, times(1)).doFilter(mockHttpServletRequest, mockHttpServletResponse);
    }

    @Test
    public void filter_shouldNotExecuteIfPathIsOnWhitelist() throws ServletException, IOException {
        when(mockHttpServletRequest.getServletPath()).thenReturn(UNAUTHORIZED_URL);
        when(mockHttpServletRequest.getMethod()).thenReturn("POST");
        requirementFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);
        verify(mockHttpServletRequest, times(0)).getUserPrincipal();
        verify(mockHttpServletResponse, times(0)).sendError(HttpServletResponse.SC_FORBIDDEN);
        verify(mockFilterChain, times(1)).doFilter(mockHttpServletRequest, mockHttpServletResponse);
    }


    public void setUpTokenMocks(String headerToken, Token actualToken) {
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));
        when(mockHttpServletRequest.getHeader("token")).thenReturn(headerToken);
        if(actualToken != null) {
            when(mockTokenService.getTokenByUserId(1L)).thenReturn(Optional.of(actualToken));
        } else {
            when(mockTokenService.getTokenByUserId(1L)).thenReturn(Optional.empty());
        }
        when(mockPrincipal.getName()).thenReturn(USERNAME);
    }

    public void setUpApiKeyMocks(String headerApiKey, String newestApiKey) {
        when(mockHttpServletRequest.getHeader("api-key")).thenReturn(headerApiKey);
        when(mockApiKeyService.getNewestApiKey()).thenReturn(newestApiKey);
    }

    public void setUpFilterMocks(String url) {
        when(mockHttpServletRequest.getServletPath()).thenReturn(url);
        when(mockHttpServletRequest.getUserPrincipal()).thenReturn(mockPrincipal);
    }

}