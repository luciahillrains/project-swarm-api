package com.bluecatlabs.projectswarm.endpoints;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.server.BackendInfoResponse;
import com.bluecatlabs.projectswarm.models.server.World;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.servers.WorldService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ServerControllerTest {

    @Mock
    ApiKeyService mockApiKeyService;

    @Mock
    WorldService mockWorldService;

    ServerController serverController;

    List<World> worldList = new ArrayList<>();
    private static final String TEST_VERSION = "testVersion";
    private static final String UI_VERSION = "uiVersion";
    private static final String BUILD_NAME = "test";
    private static final String API_KEY = "api-key";

    @BeforeEach
    public void setup() {
        serverController = new ServerController(mockApiKeyService, mockWorldService);
        ReflectionTestUtils.setField(serverController, "backendVersion", TEST_VERSION);
        ReflectionTestUtils.setField(serverController, "uiVersionSupported", UI_VERSION);
        ReflectionTestUtils.setField(serverController, "buildName", BUILD_NAME);
        World world = new World();
        world.setId(1L);
        worldList.add(world);
    }

    @Test
    public void getBackendInfo_returnsExceptionIfApiKeyNotFound() {
        when(mockApiKeyService.isApiKeyHistoric(API_KEY)).thenReturn(false);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            serverController.getBackendInfo(API_KEY, "");
        });
        assertEquals(HttpStatus.FORBIDDEN, exception.getStatusCode());
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }
    @Test
    public void getBackendInfo_returnsInfoWithTrueClientNotSupported() {
        when(mockApiKeyService.isApiKeyHistoric(API_KEY)).thenReturn(true);
        when(mockWorldService.getAllWorlds()).thenReturn(worldList);
        BackendInfoResponse response = serverController.getBackendInfo(API_KEY, "");

        assertEquals(TEST_VERSION, response.getBackendBuild(), "Backend build should match");
        assertEquals(BUILD_NAME, response.getBuildName(), "Build name should match.");
        assertEquals(worldList, response.getWorldList(), "World list should map.");
        assertTrue(response.isClientNotSupported(), "'' != UI_VERSION, so client is not supported.");
    }

    @Test
    public void getBackendInfo_returnsFalseIfCLientIsNotSupported() {
        when(mockApiKeyService.isApiKeyHistoric(API_KEY)).thenReturn(true);
        BackendInfoResponse response = serverController.getBackendInfo(API_KEY, UI_VERSION);

        assertFalse(response.isClientNotSupported(), "UI_VERSION = clientVersion so client is supported.");
    }
}