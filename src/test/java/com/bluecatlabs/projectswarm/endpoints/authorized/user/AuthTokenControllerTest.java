package com.bluecatlabs.projectswarm.endpoints.authorized.user;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.request.characters.CharacterLoginRequest;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthTokenControllerTest {

    @Mock
    TokenService mockTokenService;

    @Mock
    UserService mockUserService;

    @Mock
    Principal mockPrincipal;

    AuthTokenController controller;

    public static final String TEST_USER_NAME = "test";
    public static final long TEST_USER_ID = 1L;
    public static final long TEST_CHAR_ID = 2L;
    private final UserAccount userAccount = new UserAccount();
    private CharacterLoginRequest request = new CharacterLoginRequest();
    private PlayerCharacter playerCharacter = new PlayerCharacter();
    private Token token = new Token();
    @BeforeEach
    public void setup() {
        controller = new AuthTokenController(mockUserService, mockTokenService);
        userAccount.setId(TEST_USER_ID);
        request.setCharacterId(TEST_CHAR_ID);
        playerCharacter.setId(TEST_CHAR_ID);
        token.setUserId(TEST_USER_ID);
    }

    @Test
    public void testDelete() {
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.of(userAccount));
        doNothing().when(mockTokenService).deleteToken(TEST_USER_ID);

        controller.deleteToken(mockPrincipal);

        verify(mockTokenService, times(1)).deleteToken(TEST_USER_ID);
    }

    @Test
    public void setCharacterToToken_return401IfTokenDoesNotExistForUser() {
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.of(userAccount));
        when(mockTokenService.getTokenByUserId(userAccount.getId())).thenReturn(Optional.empty());
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            controller.setCharacterToToken(request, mockPrincipal);
        });

        assertEquals(HttpStatus.UNAUTHORIZED, exception.getStatusCode(), "Status code should be a 401.");
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }

    @Test
    public void setCharacterToToken_return401IfUserDoesntOwnCharacter() {
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.of(userAccount));
        when(mockTokenService.getTokenByUserId(userAccount.getId())).thenReturn(Optional.of(token));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            controller.setCharacterToToken(request, mockPrincipal);
        });

        assertEquals(HttpStatus.UNAUTHORIZED, exception.getStatusCode(), "Status code is 401");
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }

    @Test
    public void setCharacterToToken_returnGenResponseIfThingsGoOkay() {
        userAccount.getCharacters().add(playerCharacter);
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.of(userAccount));
        when(mockTokenService.getTokenByUserId(userAccount.getId())).thenReturn(Optional.of(token));
        when(mockTokenService.saveToken(token)).thenReturn(null);

        GeneralResponse response = controller.setCharacterToToken(request, mockPrincipal);

        assertTrue(response.isSuccess(), "Should be successful");
    }
}