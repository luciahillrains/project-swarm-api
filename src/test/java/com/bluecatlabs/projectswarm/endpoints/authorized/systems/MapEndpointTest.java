package com.bluecatlabs.projectswarm.endpoints.authorized.systems;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.map.MapDTO;
import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import com.bluecatlabs.projectswarm.models.login.Token;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MapEndpointTest {
    @Mock
    UserService mockUserService;

    @Mock
    TokenService mockTokenService;

    @Mock
    Principal mockPrincipal;

    MapEndpoint mapEndpoint;

    UserAccount userAccount;

    Token token;

    private static final String TEST_USER_NAME = "TEST";
    private static final long TEST_USER_ID = 1;
    private static final long CHARACTER_ID = 1;
    private static final int MAP_ID = 1;
    @BeforeEach
    public void setup() {
        mapEndpoint = new MapEndpoint(mockUserService, mockTokenService);
        userAccount = new UserAccount();
        userAccount.setId(1);
        PlayerCharacter character = new PlayerCharacter();
        character.setId(1);
        userAccount.setCharacters(List.of(character));
        token = new Token();
        token.setCharacter(character);
    }

    @Test
    public void getMapInformation_newUserAccountCausesException() {
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.empty());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            mapEndpoint.getMapInformation(CHARACTER_ID, MAP_ID, mockPrincipal);
        });

        assertEquals(HttpStatus.FORBIDDEN, exception.getStatusCode());
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }
    @Test
    public void getMapInformation_tokenNotAssociatedCausesException() {
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.of(userAccount));
        when(mockTokenService.getTokenByUserId(TEST_USER_ID)).thenReturn(Optional.of(new Token()));
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            mapEndpoint.getMapInformation(CHARACTER_ID, MAP_ID, mockPrincipal);
        });

        assertEquals(HttpStatus.FORBIDDEN, exception.getStatusCode());
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }


    @Test
    public void getMapInformation_shouldSucceed() {
        when(mockPrincipal.getName()).thenReturn(TEST_USER_NAME);
        when(mockUserService.findByUsername(TEST_USER_NAME)).thenReturn(Optional.of(userAccount));
        when(mockTokenService.getTokenByUserId(TEST_USER_ID)).thenReturn(Optional.of(token));
        MapDTO mapDTO = mapEndpoint.getMapInformation(CHARACTER_ID, MAP_ID, mockPrincipal);

        assertNotNull(mapDTO);
    }
}