package com.bluecatlabs.projectswarm.endpoints.authorized.character;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.characters.CharacterDTO;
import com.bluecatlabs.projectswarm.dto.request.characters.CharacterActionRequest;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import com.bluecatlabs.projectswarm.models.characters.PlayerClass;
import com.bluecatlabs.projectswarm.models.characters.PlayerRace;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.models.security.UserAuthority;
import com.bluecatlabs.projectswarm.models.server.World;
import com.bluecatlabs.projectswarm.services.characters.CharacterService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import com.bluecatlabs.projectswarm.services.servers.WorldService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CharacterEndpointTest {

    @Mock
    CharacterService mockCharacterService;

    @Mock
    UserService mockUserService;

    @Mock
    Principal mockPrincipal;

    @Mock
    WorldService mockWorldService;

    CharacterEndpoint characterEndpoint;
    CharacterDTO characterRequest = new CharacterDTO();
    UserAccount userAccount;
    CharacterActionRequest actionRequest;
    World world;
    private static final String TEST_USER_NAME = "test";
    private static final String CHARACTER_1_FIRST_NAME = "Char";
    private static final String CHARACTER_1_LAST_NAME = "One";
    private static final int CHARACTER_1_CLASS_ID = PlayerClass.ARCHER.ordinal();
    private static final int CHARACTER_1_RACE_ID = PlayerRace.ELF.ordinal();
    private static final String CHARACTER_2_FIRST_NAME = "Chara";
    private static final String CHARACTER_2_LAST_NAME = "Two";
    private static final int CHARACTER_2_CLASS_ID = PlayerClass.ELEMENTALIST.ordinal();
    private static final int CHARACTER_2_RACE_ID = PlayerRace.BIRD.ordinal();
    @BeforeEach
    public void setup() {
        characterEndpoint = new CharacterEndpoint(mockCharacterService,
                mockUserService,
                mockWorldService);
        characterRequest.setFirstName("Test");
        characterRequest.setLastName("LastTest");
        characterRequest.setClassId(1);
        characterRequest.setRaceId(1);
        characterRequest.setWorldId(1);

        world = new World();
        world.setId(1);
        userAccount = new UserAccount();
        userAccount.setAuthority(new UserAuthority());
        actionRequest = new CharacterActionRequest();
        actionRequest.setCharacterId(1L);
        actionRequest.setMapId(3L);
        ReflectionTestUtils.setField(characterEndpoint, "characterLimitRegularAccount", 4);
        ReflectionTestUtils.setField(characterEndpoint, "characterLimitPremiumAccount", 6);
        ReflectionTestUtils.setField(characterEndpoint, "characterLimitLimitedAccount", 1);


    }

    @Test
    public void testMapRequest_ThrowsExceptionIfIdPassedInHigherThanAmountOfClasses() {
        characterRequest.setClassId(9999);
        IllegalArgumentException exception =  assertThrows(IllegalArgumentException.class,
                () -> characterEndpoint.mapRequestToPlayerCharacter(characterRequest));
        assertEquals(ErrorStrings.INVALID_PLAYER_CLASS, exception.getMessage(), "Message should be INVALID_PLAYER_CLASS");
    }

    @Test
    public void testMapRequest_WorksAsExpected() {
        when(mockWorldService.getWorldById(1)).thenReturn(Optional.of(world));
        PlayerCharacter character = characterEndpoint.mapRequestToPlayerCharacter(characterRequest);
        assertEquals("Test", character.getFirstName(), "First name should be Test");
        assertEquals("LastTest", character.getLastName(), "Last name should be LastTest");
        assertEquals(PlayerClass.MEDIC, character.getBaseClass(), "Class should be Medic");
        assertEquals(PlayerRace.ELF, character.getRace(), "Race should be Elf");
        assertEquals(world.getId(), character.getWorld().getId(), "World Id should be equal to their world.");
    }

    @Test
    public void testMapRequest_ThrowsExceptionIfIDPassedInHigherThanAmountOfRaces() {
        characterRequest.setRaceId(9999);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> characterEndpoint.mapRequestToPlayerCharacter(characterRequest));
        assertEquals(ErrorStrings.INVALID_RACE, exception.getMessage(), "Message should be INVALID_RACE");
    }

    @Test
    public void testMapRequest_ThrowsExceptionIfWorldIsNotFound() {
        when(mockWorldService.getWorldById(1)).thenReturn(Optional.empty());
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> characterEndpoint.mapRequestToPlayerCharacter(characterRequest));
        assertEquals(ErrorStrings.WORLD_NOT_FOUND, exception.getMessage(), "Message should be WORLD_NOT_FOUND");
    }

    @Test
    public void testCreateCharacter_ThrowsExceptionIfRequestIsBad() {
        characterRequest.setRaceId(9999);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> characterEndpoint.createCharacter(characterRequest, mockPrincipal));
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode(), "Exception should be a 400.");
        assertEquals(ErrorStrings.BAD_VALUES_IN_CHARACTER_REQUESTS, exception.getReason());

    }

    @Test
    public void testCreateCharacter_WorksAsExpected() {
        when(mockPrincipal.getName()).thenReturn("test");
        when(mockWorldService.getWorldById(1)).thenReturn(Optional.of(world));
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));
        when(mockUserService.save(userAccount)).thenReturn(null);

        GeneralResponse response = characterEndpoint.createCharacter(characterRequest, mockPrincipal);

        assertTrue(response.isSuccess(), "Response should be successful");

    }

    @Test
    public void testCreateCharacter_Throws400IfCharacterRequestInvalid() {
        characterRequest.setRaceId(9999);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> {
            characterEndpoint.createCharacter(characterRequest, mockPrincipal);
        });
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals(ErrorStrings.BAD_VALUES_IN_CHARACTER_REQUESTS, responseStatusException.getReason());
    }

    @Test
    public void testCreateCharacter_Returns400IfCharacterMeetsLimits() {
        userAccount.setCharacters(seedCharacters(9));
        userAccount.setAuthority(new UserAuthority());
        when(mockPrincipal.getName()).thenReturn("test");
        when(mockWorldService.getWorldById(1)).thenReturn(Optional.of(world));
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));

        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> {
            characterEndpoint.createCharacter(characterRequest, mockPrincipal);
        });

        assertEquals(HttpStatus.BAD_REQUEST, ex.getStatusCode());
        assertEquals(ErrorStrings.TOO_MANY_CHARACTERS, ex.getReason());
    }

    @Test
    public void testListCharacter_ReturnsAListOfDTO() {
        userAccount.setCharacters(seedCharacters());
        when(mockPrincipal.getName()).thenReturn("test");
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));
        List<CharacterDTO> dtos = characterEndpoint.getCharacterList(mockPrincipal);

        assertEquals(2, dtos.size(), "There should be two entries in the list.");
        assertEquals(1L, dtos.get(0).getCharacterId(), "[1] ID should exist and be correct.");
        assertEquals(CHARACTER_1_FIRST_NAME, dtos.get(0).getFirstName(), "[1]First name should be correct.");
        assertEquals(CHARACTER_1_LAST_NAME, dtos.get(0).getLastName(), "[1]Last name should be correct.");
        assertEquals(CHARACTER_1_CLASS_ID, dtos.get(0).getClassId(), "[1]Class id should be correct.");
        assertEquals(CHARACTER_1_RACE_ID, dtos.get(0).getRaceId(), "[1]Race id name should be correct.");
        assertEquals(world.getId(), dtos.get(0).getWorldId(), "[1] World id should be correct.");
        assertEquals(2L, dtos.get(1).getCharacterId(), "[2] ID should exist and be correct.");
        assertEquals(CHARACTER_2_FIRST_NAME, dtos.get(1).getFirstName(), "[2]First name should be correct.");
        assertEquals(CHARACTER_2_LAST_NAME, dtos.get(1).getLastName(), "[2]Last name should be correct.");
        assertEquals(CHARACTER_2_CLASS_ID, dtos.get(1).getClassId(), "[2]Class id should be correct.");
        assertEquals(CHARACTER_2_RACE_ID, dtos.get(1).getRaceId(), "[2]Race id name should be correct.");
        assertEquals(world.getId(), dtos.get(0).getWorldId(), "[2] World id should be correct.");
    }

    @Test
    public void moveCharacterToMap_Returns500IfNoUserFound() {
        when(mockPrincipal.getName()).thenReturn("blah");
        when(mockUserService.findByUsername("blah")).thenReturn(Optional.empty());
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            characterEndpoint.moveCharacterToMap(null, mockPrincipal);
        });
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getStatusCode(), "No user found is 500.");
        assertEquals(ErrorStrings.BAD_USER_IN_PRINCIPAL, exception.getReason());
    }

    @Test
    public void moveCharacterToMap_CharacterNotOwnedThrows401() {
        userAccount.setCharacters(seedCharacters());
        actionRequest.setCharacterId(99L);
        when(mockPrincipal.getName()).thenReturn("test");
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            characterEndpoint.moveCharacterToMap(actionRequest, mockPrincipal);
        });

        assertEquals(HttpStatus.UNAUTHORIZED, exception.getStatusCode(), "Trying to manipulate a character thatdoesn't belong to user is a 401.");
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }

    @Test
    public void moveCharacterToNap_MapIDInvalidThrows400() {
        userAccount.setCharacters(seedCharacters());
        actionRequest.setMapId(-1L);
        when(mockPrincipal.getName()).thenReturn("test");
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            characterEndpoint.moveCharacterToMap(actionRequest, mockPrincipal);
        });
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode(), "Invalid map idea throws 401.");
        assertEquals(ErrorStrings.BAD_VALUES_IN_CHARACTER_REQUESTS, exception.getReason());
    }

    @Test
    public void moveCharacterToMap_Success() {
        userAccount.setCharacters(seedCharacters());
        when(mockPrincipal.getName()).thenReturn("test");
        when(mockUserService.findByUsername("test")).thenReturn(Optional.of(userAccount));
        when(mockCharacterService.save(isA(PlayerCharacter.class))).thenReturn(null);

        GeneralResponse response = characterEndpoint.moveCharacterToMap(actionRequest, mockPrincipal);

        assertTrue(response.isSuccess());
    }

    @Test
    public void testIsPlayerAboveCharacterLimit_ForPremiumAccountReturnsFalseFor2Characters() {
            userAccount.setCharacters(seedCharacters());
            UserAuthority authority = new UserAuthority();
            authority.setTipper(true);
            userAccount.setAuthority(authority);

            boolean actual = characterEndpoint.isPlayerAboveCharacterLimit(userAccount);

            assertFalse(actual, "2 characters is less than limit.");
    }

    @Test
    public void testIsPlayerAboveCharacterLimit_ForPremiumAccountReturnsTrueForALotOfCharacters() {
        userAccount.setCharacters(seedCharacters(9));
        UserAuthority authority = new UserAuthority();
        authority.setTipper(true);
        userAccount.setAuthority(authority);
        boolean actual = characterEndpoint.isPlayerAboveCharacterLimit(userAccount);

        assertTrue(actual, "9 characters is more than limit.");
    }



    @Test
    public void testIsPlayerAboveCharacterLimit_ForRegularAccountReturnsFalseFor2Characters() {
        userAccount.setCharacters(seedCharacters());
        userAccount.setAuthority(new UserAuthority());

        boolean actual = characterEndpoint.isPlayerAboveCharacterLimit(userAccount);

        assertFalse(actual, "2 characters is less than limit.");
    }
    @Test
    public void testIsPlayerAboveCharacterLimit_ForUserAccountReturnsTrueForALotOfCharacters() {
        userAccount.setCharacters(seedCharacters(5));
        userAccount.setAuthority(new UserAuthority());

        boolean actual = characterEndpoint.isPlayerAboveCharacterLimit(userAccount);

        assertTrue(actual, "5 characters is more than limit.");
    }

    public List<PlayerCharacter> seedCharacters() {
        List<PlayerCharacter> characters = new ArrayList<>();

        PlayerCharacter char1 = new PlayerCharacter();
        char1.setId(1L);
        char1.setFirstName(CHARACTER_1_FIRST_NAME);
        char1.setLastName(CHARACTER_1_LAST_NAME);
        char1.setBaseClass(PlayerClass.ARCHER);
        char1.setRace(PlayerRace.ELF);
        char1.setWorld(world);
        characters.add(char1);

        PlayerCharacter char2 = new PlayerCharacter();
        char2.setId(2L);
        char2.setFirstName(CHARACTER_2_FIRST_NAME);
        char2.setLastName(CHARACTER_2_LAST_NAME);
        char2.setBaseClass(PlayerClass.ELEMENTALIST);
        char2.setRace(PlayerRace.BIRD);
        char2.setWorld(world);
        characters.add(char2);

        return characters;
    }

    public List<PlayerCharacter> seedCharacters(int times) {
        List<PlayerCharacter> characters = new ArrayList<>();
        for(int i = 0; i < times; i++) {
            PlayerCharacter pc = new PlayerCharacter();
            pc.setId(i);
            characters.add(pc);
        }
        return characters;
    }



}