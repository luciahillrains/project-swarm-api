package com.bluecatlabs.projectswarm.endpoints.authorized.user;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.useraccounts.UserAccountResponse;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class AuthUserAccountControllerTest {
    @Mock
    UserService mockUserService;

    @Mock
    Principal mockPrincipal;
    AuthUserAccountController controller;
    UserAccount userAccount;

    private static final String TEST_USERNAME = "TEST_USER";
    private static final String TEST_EMAIL = "TEST_EMAIL";
    @BeforeEach
    public void setup() {
        controller = new AuthUserAccountController(mockUserService);
        userAccount = new UserAccount();
        userAccount.setEmail(TEST_EMAIL);
        userAccount.setUsername(TEST_USERNAME);
    }

    @Test
    public void getAccountDetails_returnExceptionIfUserAccountNotFound() {
        when(mockPrincipal.getName()).thenReturn(TEST_USERNAME);
        when(mockUserService.findByUsername(TEST_USERNAME)).thenReturn(Optional.empty());
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            controller.getAccountDetails(mockPrincipal);
        });
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getStatusCode(), "HttpCode should be 500");
        assertEquals(ErrorStrings.BAD_USER_IN_PRINCIPAL, exception.getReason());
    }

    @Test
    public void getAccountDetails_returnResponseIfOK() {
        when(mockPrincipal.getName()).thenReturn(TEST_USERNAME);
        when(mockUserService.findByUsername(TEST_USERNAME)).thenReturn(Optional.of(userAccount));
        UserAccountResponse response = controller.getAccountDetails(mockPrincipal);
        assertEquals(TEST_USERNAME, response.getUsername(), "Response should match user name");
        assertEquals(TEST_EMAIL, response.getEmail(), "Response should match email.");
    }
}