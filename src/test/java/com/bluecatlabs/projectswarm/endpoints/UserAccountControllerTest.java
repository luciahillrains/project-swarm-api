package com.bluecatlabs.projectswarm.endpoints;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.request.useraccounts.NewUserAccountRequest;
import com.bluecatlabs.projectswarm.dto.response.GeneralResponse;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import com.bluecatlabs.projectswarm.services.servers.ServerSwitchService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserAccountControllerTest {

    @Mock
    UserService mockUserService;

    @Mock
    ServerSwitchService mockServerSwitchService;

    @Mock
    ApiKeyService mockApiKeyService;

    UserAccountController userAccountController;
    NewUserAccountRequest newUserAccountRequest;
    private static final String TEST_USER = "TEST_USER";
    private static final String TEST_EMAIL = "TEST@EMAIL.COM";
    private static final String TEST_PASSWORD = "TESTPASSWORD";
    private static final String API_KEY = "API_KEY";
    @BeforeEach
    public void setup() {
        userAccountController = new UserAccountController(mockUserService,
                mockServerSwitchService,
                mockApiKeyService);
        newUserAccountRequest = new NewUserAccountRequest();
        newUserAccountRequest.setUsername(TEST_USER);
        newUserAccountRequest.setPassword(TEST_PASSWORD);
        newUserAccountRequest.setEmail(TEST_EMAIL);
    }

    @Test
    public void testUserAccount_CreateNormalUserWithPrereleaseSwitchOff() {
        when(mockServerSwitchService.isSwitchActive(ServerSwitchService.PRERELEASE_SWITCH)).thenReturn(false);
        UserAccount userAccount = userAccountController.mapToUserAccount(newUserAccountRequest);
        assertEquals(TEST_USER, userAccount.getUsername(), "Username should be the same.");
        assertEquals(TEST_EMAIL, userAccount.getEmail(), "email should be the same.");
        assertEquals(TEST_PASSWORD, userAccount.getPassword(), "Password should be the same.");
        assertFalse(userAccount.getAuthority().isPrerelease(), "Prerelease should be false");
    }

    @Test
    public void testUserAccount_CreatePrereleaseUserWithPrereleaseSwitchOn() {
        when(mockServerSwitchService.isSwitchActive(ServerSwitchService.PRERELEASE_SWITCH)).thenReturn(true);
        UserAccount userAccount = userAccountController.mapToUserAccount(newUserAccountRequest);
        assertTrue(userAccount.getAuthority().isPrerelease(), "Prerelease should be true.");
    }

    @Test
    public void createAccount_sendsErrorIfAPIKeyIsNotNewest() {
        when(mockApiKeyService.getNewestApiKey()).thenReturn("");
        ResponseStatusException exception =  assertThrows(ResponseStatusException.class, () -> {
            userAccountController.createAccount(API_KEY, newUserAccountRequest);
        });
        assertEquals(HttpStatus.FORBIDDEN, exception.getStatusCode());
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }

    @Test
    public void createAccount_sendsErrorIfAccountIsNonUnique() throws ResponseStatusException {
        when(mockApiKeyService.getNewestApiKey()).thenReturn(API_KEY);
        when(mockServerSwitchService.isSwitchActive(ServerSwitchService.PRERELEASE_SWITCH)).thenReturn(true);
        when(mockUserService.saveNewUserAccount(isA(UserAccount.class))).thenReturn(false);
        ResponseStatusException exception =  assertThrows(ResponseStatusException.class, () -> {
            userAccountController.createAccount(API_KEY, newUserAccountRequest);
        });

        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode(), "Request should be a 400.");
        assertEquals(ErrorStrings.NONUNIQUE_USER, exception.getReason());
    }

    @Test
    public void createAccount_sendsSuccessIfIsSaved() {
        when(mockApiKeyService.getNewestApiKey()).thenReturn(API_KEY);
        when(mockServerSwitchService.isSwitchActive(ServerSwitchService.PRERELEASE_SWITCH)).thenReturn(false);
        when(mockUserService.saveNewUserAccount(isA(UserAccount.class))).thenReturn(true);
        GeneralResponse response = userAccountController.createAccount(API_KEY, newUserAccountRequest);
        assertTrue(response.isSuccess(), "Should be successful.");
    }
}