package com.bluecatlabs.projectswarm.endpoints;

import com.bluecatlabs.projectswarm.config.ErrorStrings;
import com.bluecatlabs.projectswarm.dto.response.token.TokenResponse;
import com.bluecatlabs.projectswarm.exceptions.AlreadyOwnsATokenException;
import com.bluecatlabs.projectswarm.models.security.UserAccount;
import com.bluecatlabs.projectswarm.services.login.ApiKeyService;
import com.bluecatlabs.projectswarm.services.login.TokenService;
import com.bluecatlabs.projectswarm.services.login.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TokenControllerTest {

    @Mock
    UserService mockUserService;

    @Mock
    TokenService mockTokenService;

    @Mock
    Principal mockPrincipal;

    @Mock
    ApiKeyService mockApiKeyService;

    TokenController tokenController;
    UserAccount testAccount = new UserAccount();

    private static final String TEST_USERNAME = "username";
    private static final String TEST_TOKEN = "TESTTOKEN";
    private static final String TEST_API_KEY = "TESTKEY";

    @BeforeEach
    public void setup() {
        tokenController = new TokenController(mockUserService, mockTokenService, mockApiKeyService);
        testAccount.setId(1L);
        when(mockPrincipal.getName()).thenReturn(TEST_USERNAME);
        when(mockUserService.findByUsername(TEST_USERNAME)).thenReturn(Optional.of(testAccount));
    }

    @Test
    public void createToken_ShouldReturnExceptionIfUserIsReprimanded() throws AlreadyOwnsATokenException {
        when(mockApiKeyService.isApiKeyHistoric(TEST_API_KEY)).thenReturn(true);
        when(mockUserService.hasReprimandedAccount(testAccount)).thenReturn(true);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            tokenController.createToken(TEST_API_KEY, mockPrincipal);
        });

        assertEquals(HttpStatus.FORBIDDEN, exception.getStatusCode(), "Response should have 403 error code");
        assertEquals(ErrorStrings.USER_HAS_REPRIMAND, exception.getReason(), "Response should have user reprimanded message.");
    }

    @Test
    public void createToken_shouldReturnErrorResponseIfUserAlreadyHasToken() throws AlreadyOwnsATokenException {
        when(mockTokenService.generateNewToken(1L)).thenThrow(AlreadyOwnsATokenException.class);
        when(mockApiKeyService.isApiKeyHistoric(TEST_API_KEY)).thenReturn(true);
        when(mockUserService.hasReprimandedAccount(testAccount)).thenReturn(false);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            tokenController.createToken(TEST_API_KEY, mockPrincipal);
        });
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode(), "Response should be a 400");
        assertEquals(ErrorStrings.USER_ALREADY_AUTHORIZED, exception.getReason(), "Response should explain user is already logged in.");

    }

    @Test
    public void createToken_returns400IfUserAPIKeyIsNotFound() throws AlreadyOwnsATokenException {
        when(mockApiKeyService.isApiKeyHistoric(TEST_API_KEY)).thenReturn(false);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            tokenController.createToken(TEST_API_KEY, mockPrincipal);
        });
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode(), "Response should be 400");
        assertEquals(ErrorStrings.UNAUTHORIZED_REQUEST, exception.getReason());
    }

    @Test
    public void createToken_shouldReturnTokenInResponse() throws AlreadyOwnsATokenException {
        when(mockTokenService.generateNewToken(1l)).thenReturn(TEST_TOKEN);
        when(mockApiKeyService.isApiKeyHistoric(TEST_API_KEY)).thenReturn(true);
        when(mockUserService.hasReprimandedAccount(testAccount)).thenReturn(false);
        TokenResponse response = tokenController.createToken(TEST_API_KEY, mockPrincipal);
        assertEquals(TEST_TOKEN, response.getToken(), "Response should contain a token.");
    }

}