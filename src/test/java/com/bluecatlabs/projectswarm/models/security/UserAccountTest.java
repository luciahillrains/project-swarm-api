package com.bluecatlabs.projectswarm.models.security;

import com.bluecatlabs.projectswarm.models.characters.PlayerCharacter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Date;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

public class UserAccountTest {

    UserAccount userAccount;

    List<Reprimand> reprimandList;
    List<PlayerCharacter> characterList;

    @BeforeEach
    public void setup() {
        userAccount = new UserAccount();
        reprimandList = new ArrayList<>();
        characterList = new ArrayList<>();
    }

    @Test
    public void getActiveReprimands_getAllActiveReprimandsOfHighestLevel() {
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_1, Calendar.getInstance().getTime(), true));
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_2, Calendar.getInstance().getTime(), true));
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_1, Calendar.getInstance().getTime(), false));
        userAccount.setReprimands(reprimandList);

        Optional<Reprimand> actual = userAccount.getActiveReprimandOfHighestLevel();

        assertTrue(actual.isPresent(), "There should be an active Reprimand");
        assertEquals(ReprimandLevel.LEVEL_2, actual.get().getLevel(), "Should be a Level 2 Suspension");
    }

    @Test
    public void getActiveReprimands_shouldReturnEmptyIfNoActiveLevel() {
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_1, Calendar.getInstance().getTime(), false));
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_1, Calendar.getInstance().getTime(), false));
        userAccount.setReprimands(reprimandList);

        Optional<Reprimand> actual = userAccount.getActiveReprimandOfHighestLevel();

        assertTrue(actual.isEmpty(), "There should not be an active reprimand");
    }

    @Test
    public void getActiveReprimands_shouldReturnAReprimandIfAllOfSameLevel() {
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_4, Calendar.getInstance().getTime(), true));
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_4, Calendar.getInstance().getTime(), true));
        reprimandList.add(createReprimand(ReprimandLevel.LEVEL_2, Calendar.getInstance().getTime(), true));
        userAccount.setReprimands(reprimandList);

        Optional<Reprimand> actual = userAccount.getActiveReprimandOfHighestLevel();

        assertTrue(actual.isPresent(), "There should be a reprimand active");
        assertEquals(ReprimandLevel.LEVEL_4, actual.get().getLevel(), "Should be LEVEL_4_BAN");
    }

    @Test
    public void getCharacter_shouldReturnCharacter() {
        characterList.add(createCharacter(0, "test1", "test1"));
        characterList.add(createCharacter(1, "test2", "test2"));
        characterList.add(createCharacter(2, "test3", "test3"));
        userAccount.setCharacters(characterList);

        Optional<PlayerCharacter> actual = userAccount.getCharacter(1);

        assertTrue(actual.isPresent(), "There is an existing character.");
        assertEquals(actual.get().getId(), 1, "ID is 1");
        assertEquals(actual.get().getFirstName(), "test2", "First name is Test2");
    }

    @Test
    public void getCharacter_returnsEmptyIfCharacterNotFound() {
        characterList.add(createCharacter(0, "test1", "test1"));
        characterList.add(createCharacter(1, "test2", "test2"));
        characterList.add(createCharacter(2, "test3", "test3"));
        userAccount.setCharacters(characterList);

        Optional<PlayerCharacter> actual = userAccount.getCharacter(3);

        assertTrue(actual.isEmpty(), "Actual should be empty.");
    }

    @Test
    public void getCharacter_doesntBreakWithEmptyCharacterList() {
        userAccount.setCharacters(characterList);

        Optional<PlayerCharacter> actual = userAccount.getCharacter(1);
        assertTrue(actual.isEmpty(), "Actual should be empty.");
    }

    public static Reprimand createReprimand(ReprimandLevel level, Date date, boolean active) {
        Reprimand reprimand = new Reprimand();
        reprimand.setLevel(level);
        reprimand.setCreated(date);
        reprimand.setActive(active);
        return reprimand;
    }

    public static PlayerCharacter createCharacter(long id, String firstName, String lastName) {
        PlayerCharacter playerCharacter = new PlayerCharacter();
        playerCharacter.setId(id);
        playerCharacter.setFirstName(firstName);
        playerCharacter.setLastName(lastName);
        return playerCharacter;
    }

}