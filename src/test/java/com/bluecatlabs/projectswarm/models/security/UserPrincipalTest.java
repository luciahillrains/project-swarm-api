package com.bluecatlabs.projectswarm.models.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserPrincipalTest {

    UserAccount fakeUser;
    @BeforeEach
    void setup() {
        fakeUser = new UserAccount();
        fakeUser.setId(1);
    }

    @Test
    void getAuthorities_USER_ROLE() {
        UserPrincipal userPrincipal = createPrincipal(false, false, false, false);
        List<GrantedAuthority> authorityList = (List<GrantedAuthority>) userPrincipal.getAuthorities();
        assertEquals(1, authorityList.size(), "User role should have one granted authority");
        assertEquals("USER", authorityList.get(0).getAuthority(), "User authority should be 'USER'");
    }

    @Test
    void getAuthorities_GM_ROLE() {
        UserPrincipal userPrincipal = createPrincipal(false, true, true, true);
        List<GrantedAuthority> authorityList = (List<GrantedAuthority>) userPrincipal.getAuthorities();
        assertEquals(4, authorityList.size(), "GM role should have two granted authority");
        assertEquals("GAME_MASTER", authorityList.get(0).getAuthority(), "GM authority should have 'GAME_MASTER'");
        assertEquals("PREMIUM", authorityList.get(1).getAuthority(), "GM authority should have 'PREMIUM'");
        assertEquals("PRERELEASE", authorityList.get(2).getAuthority(), "GM authority should have 'PRERELEASE'");
        assertEquals("USER", authorityList.get(3).getAuthority(), "GM authority should have 'USER'");
    }

    @Test
    void getAuthorities_ADMIN_ROLE() {
        UserPrincipal userPrincipal = createPrincipal(true, true, true, true);
        List<GrantedAuthority> authorityList = (List<GrantedAuthority>) userPrincipal.getAuthorities();
        assertEquals(5, authorityList.size(), "Admin role should have three granted authority");
        assertEquals("ADMIN", authorityList.get(0).getAuthority(), "Admin authority should have 'ADMIN'");
        assertEquals("GAME_MASTER", authorityList.get(1).getAuthority(), "Admin authority should have 'GAME_MASTER'");
        assertEquals("PREMIUM", authorityList.get(2).getAuthority(), "Admin authority should have 'PREMIUM'");
        assertEquals("PRERELEASE", authorityList.get(3).getAuthority(), "Admin authority should have 'PRERELASE'");
        assertEquals("USER", authorityList.get(4).getAuthority(), "Admin authority should have 'USER'");
    }

    @Test
    void getAuthorities_PREMIUM_ROLE() {
        UserPrincipal userPrincipal = createPrincipal(false, false, false, true);
        List<GrantedAuthority> authorityList = (List<GrantedAuthority>) userPrincipal.getAuthorities();
        assertEquals(2, authorityList.size(), "Premium role should have two authorities.");
        assertEquals("PREMIUM", authorityList.get(0).getAuthority(), "Premium role should have PREMIUM role.");
        assertEquals("USER", authorityList.get(1).getAuthority(), "Premium role should have USER role.");
    }

    @Test
    void getAuthorities_PRERELEASE_ROLE() {
        UserPrincipal userPrincipal = createPrincipal(false, false, true, false);
        List<GrantedAuthority> authorityList = (List<GrantedAuthority>) userPrincipal.getAuthorities();
        assertEquals(2, authorityList.size(), "Prerelease role should have two authorities.");
        assertEquals("PRERELEASE", authorityList.get(0).getAuthority(), "Prerelease role should have PRERELEASE role.");
        assertEquals("USER", authorityList.get(1).getAuthority(), "Prerelease role should have USER role.");
    }


    UserPrincipal createPrincipal(boolean isAdmin, boolean isGM, boolean isPrerelease, boolean isTipper) {
        UserAuthority authority = new UserAuthority();
        authority.setGM(isGM);
        authority.setAdmin(isAdmin);
        authority.setPrerelease(isPrerelease);
        authority.setTipper(isTipper);
        fakeUser.setAuthority(authority);
        return new UserPrincipal(fakeUser);
    }
}