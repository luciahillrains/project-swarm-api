# Project Swarm Changelog
Changelog of the progress.

This changelog is specifically for server-sided changes.  The actual published change log will
be stores in a different place.

## Pre-alpha 3 (0.0.3009-pa3) [Charlie] 
- Tasks
  - Refactor the requirements filter
  - Endpoints & Filters consume services and not repositories
  - Error strings are static and final.
- Bug fixes
  - Get Account details has security around it
  - Create Account endpoints returns a 201
  - Fix response exceptions not returning reasons.
- New Things
  - Added character creation
  - Added get character endpoint
  - Added endpoint to associate a character with a token.
  - Added endpoint to move character to a new map.
  - Added metadata paradigm
    - Classes and Races have metadata attached to it

## Pre-alpha 2 (0.0.2014-pa2) [Bravo]
- Logout API
- Tokens keep track of the last action with said token.
- Better logging
- Added the concept of a server switch, allowing the server to have various global states.
- Added three new user roles: PRERELEASE, PREMIUM and PREX.
- Added email to user account table.
- Added ability to sign up for an account.
- All HTTP responses send the appropiate code.
- Use context paths that are versioned.
- Added a get server endpoint.
- Added a get account info endpoint.

## Pre-alpha 1 (0.0.10X) [Alice]
- Setup server infrastructure
- Token flow
- Setup API Key recognition
- Setup authorization concerns
- Account reprimands (users can't log in if they are reprimanded)
- Account reprimands go inactive if reprimand expires on login