# Project Swarm Roadmap

### Pre-alpha 2 (0.0.2X) 
- Be able to log out & auto log out
- Add more logging
- Current code refactoring
- Create account API
- Add additional roles
- Prerelease switch

### Pre-alpha 3 (0.0.3X/0.0.1XUI) 
- Characters & Classes
- Map Movement
- The basics of the UI

### Alpha 1 (0.0.4X/0.0.15UI)
- Polling
- Maintenance Mode
- Update enhancements
- Multiserver Infrastructure
- Spawn Icons
- Tiles can cause damage


### Alpha 2 (0.0.45X/0.0.2XUI) 
- Database Import/Export mechanics
- Social features (basic Chat/Friendlist/Blacklist)

### Alpha 3 (0.0.5X/0.0.25XUI) 
- Support system

### Alpha 4 (0.0.55X/0.0.3XUI) 
- Character rename
- GM/Admin account moderation features
- 
### Alpha 5 (0.0.6X/0.0.35XUI)
- Linked maps
- Changes to polling
- Chat enhancements
- Music

### Alpha 6 (0.0.65X/0.0.4XUI)
- Stats
- GM/Admin account moderation features

### Alpha 7 (0.0.7X/0.0.45XUI)
- Admin account moderation features
- Poll updates
- Advanced Update

### Alpha 8 (0.0.75X/0.0.5XUI)
- Items
- Equipments
- Stats enhancement

### Alpha 9 (0.0.8X/0.0.55UI)
- Map Spawns
- Character deletion & restrictions

### Alpha 10 (0.0.85X/0.0.6XUI)
- Artwork
- Map enhancements

### Alpha 11 (0.0.9X/0.0.65XUI)
- Quests
- Account moderation enhancements

### Pre-Beta (0.0.95X)
- SSL
- Terms & Conditions for use
- Setup CI/CD and go live

### Beta 1 (0.1.X)
- Items & Equipment enhancements
- GM features
- Delete account functionality

### Beta 2 (0.1.5X)
- Battles
- Skills

### Beta 3 (0.2.X) 
- Skill enhancements
- Traits/Passives
- Database enhancements
- Big map/mini map
- Display ping on UI

### Beta 4 (0.2.5X)
- Buffs/debuffs added
- Mail system for GM/Admins to players

### Beta 5 (0.3.X)
- Currency added
- Shops

### Beta 6 (0.3.5X)
- Pets
- Class refactors & additional classes
- Guild Hall system (learn skills from other classes)
- Switch to "advanced" classes (maybe)
- Battle animations
- Restore account

### Beta 7 (0.4.X)
- Bigger map support
- Map camera
- Private and instanced maps
- GM/Admin account moderation enhancements
- Floors can cause buffs/debuffs

### Beta 8 (0.4.5X)
- party with friends
- multiplayer battles
- restore account

### Beta 9 (0.5.X) 
- GM Map
- New player experience

### Beta 10 (0.5.5X)
- Create friendship circles
- Friendship circle chat channel

### Beta 11 (0.6.X)
- Knowledge base
- gathering
- crafting
- equipment enhancements

### Beta 12 (0.6.5X)
- change character appearance
- equipment affects character appearance

### Beta 14 (0.7.0)
- day/night cycle
- UI refresh

### Beta 15 (0.7.5X)
- more GM modifications
- story quest from start to finish
- Helper circle
- Buff modification

### Prerelease (0.9.0)
- more gathering/crafting
- last minute modifications

### First release (1.0.0)
- allow trial players
- balance changes
